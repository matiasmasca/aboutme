---
layout: projects
title: Yet Another Nerd Education
locale_overrides:
  en:
    title: Yet Another Nerd Education
    section_titles:
      tl_dr:
        title: 'TL;DR'
        items:
          item_1: I got a University degree
          item_2: I speak Spanish & English
          item_3: I take some interesting courses (in english and spanish)
          item_4: As Student I wrote 2 papers and win the 3rd place for and Award
      university: 'University'
      degree: 'Degree'
      degree_name: 'Bachelor of Information Systems'
      gpa: 'Grade point average'
      gpa_scale: 'of 10'
      final_project: 'Final project'
      extra_activities: 'Extracurricular activities'
      languages: 'LANGUAGES'
      other_activities: OTHER ACTIVITIES
      courses: COURSES AS “**ATTENDANT**”
      academic: Academic publishing
      awards: Awards and honors
      looking_for: What I am looking for?
      read_book:
        title: Good reads
        description_text: Last books read in English
        technical_review: Technical review
  es:
    title: Otra educación nerd más
    section_titles:
      tl_dr:
        title: 'Resumen Ejecutivo'
        items:
          item_1: Obtuve un titulo universitario
          item_2: Hablo en Español y Inglés
          item_3: Tomé algunos cursos interesantes (en Inglés y en Español)
          item_4: Como Estudiante, escribí 2 papers y gane el 3re lugar en un concurso de ensayos.
      university: 'UNIVERSIDAD'
      degree: 'Titulo de grado'
      degree_name: 'Licenciado en Sistemas de Información'
      gpa: 'Promedio academico'
      gpa_scale: 'de 10'
      final_project: 'Trabajo Final de Carrera'
      extra_activities: 'Actividades extracurriculares'
      languages: 'IDIOMAS'
      other_activities: OTRAS ACTIVIDADES DE CAPACITACIÓN
      courses: CURSOS COMO “**ASISTENTE**”
      academic: Publicaciones Academicas
      awards: Premios y honores
      personal_characteristics: Características personales
      read_book:
        title: Buenas lecturas
        description_text: Últimos libros leídos
        technical_review: Revisión Técnica
  pt:
    title: Mais uma educação nerd
    section_titles:
      tl_dr:
        title: 'Sumário executivo'
        items:
          item_1: Eu tenho um diploma universitário
          item_2: Eu falo espanhol e inglês
          item_3: Eu faço alguns cursos interessantes (em inglês e espanhol)
          item_4: As Student I wrote 2 papers and win the 3rd place for and Award
      university: 'Universidade'
      degree: 'Diploma'
      degree_name: 'Bacharel em sistemas de informação'
      gpa: 'Média de notas'
      gpa_scale: 'de 10'
      final_project: 'Projeto final'
      extra_activities: 'Atividades extracurriculares'
      languages: 'Idiomas'
      other_activities: OUTRAS ATIVIDADES
      courses: CURSOS COMO “**ATENDENTE**”
      academic: Publicação acadêmica
      awards: Premios e honras
      read_book:
        title: Boas leituras
        description_text: últimos livros lidos
        technical_review: Revisao Tecnica

---
# <%= "#{resource.data.section_titles.tl_dr.title.to_s}" %>
- <%= "#{resource.data.section_titles.tl_dr.items.item_1.to_s}" %>
- <%= "#{resource.data.section_titles.tl_dr.items.item_2.to_s}" %>
- <%= "#{resource.data.section_titles.tl_dr.items.item_3.to_s}" %>
- <%= "#{resource.data.section_titles.tl_dr.items.item_4.to_s}" %>

# <%= "#{resource.data.section_titles.university.to_s}" %>
**2009 – 2015**
**Universidad Nacional del Nordeste** - Facultad de Ciencias Exactas Naturales y Agrimensura.
Corrientes city. Argentina

### <%= "#{resource.data.section_titles.degree.to_s}" %> “**<%= "#{resource.data.section_titles.degree_name.to_s}" %>**”.
**<%= "#{resource.data.section_titles.gpa.to_s}" %>**: 8.02 (<%= "#{resource.data.section_titles.gpa_scale.to_s}" %>)

**<%= "#{resource.data.section_titles.final_project.to_s}" %>**: Development of multi-platform software using BDD, TDD, and Continuous Integration. \
Main technology: Ruby / Ruby on Rails

**<%= "#{resource.data.section_titles.extra_activities.to_s}" %>**: I organized different activities and community tech events.

# <%= "#{resource.data.section_titles.languages.to_s}" %>

🇪🇦 🇦🇷 **Spanish –** Native proficiency.

🇺🇲 **English** – Limited working proficiency.

- I **write code** in English.
- Speaking: limited | Reading: advanced | Listening: pre-intermediate | Writing: pre-intermediate.

- **Technical English:** Reading: advance. Wide vocabulary.

🇧🇷 **Portugue –** Basic.

# <%= "#{resource.data.section_titles.other_activities.to_s}" %>
## <%= "#{resource.data.section_titles.courses.to_s}" %>
-   Course “Software Testing: Research and Practice”.
	- With a duration of 12:30 hrs. 19 - 24 Febrary 2018. Summer CS School RIO 2018. UNRC. Rio Cuarto city, Argentina.
-   Pos-grade course “Agile management of software projects”.
	- With a duration of 40 hrs. 02 - 24 June 2017. Corrientes city, Argentina.

-   Course “Agile Methodologies”. With a duration of 80 hrs. From April 7 to July 21, 2015. Polo IT Chaco. Resistencia city, Argentina.

-   Online Course  “**Web Application Architectures**”.
	- **New Mexico UC.** University of New Mexico through Coursera. May 2015.

-  Online Courses **“Software as a Service**” (CS169.1x & CS169.2x).
	- **Berkeley UC.** The University of California, Berkeley through edX. November 20th, 2013

-   Course “Improvements to software development processes: Introduction to level 2 of CMMI DEV version 1.2”.
	- With a duration of 50 hrs.  June 2007. Laboratorio de Calidad en Tecnologías de la Información (INTI Rosario).


## <%= "#{resource.data.section_titles.academic.to_s}" %>
- Paper: “**Desarrollo de una aplicación web utilizando Desarrollo Guiado por Comportamiento e Integración Continua”**.
	- Septiembre 2015,
	- 44º JAIIO – EST 2015
	- [http://sedici.unlp.edu.ar/handle/10915/59900](http://sedici.unlp.edu.ar/handle/10915/59900)
- Paper: “**Experiencia de recuperación de alumnos que adeudan el trabajo final en la Licenciatura en Sistemas de Información de la UNNE”**.
	- Junio 2016.
	- TE&ET XI
	- [http://sedici.unlp.edu.ar/handle/10915/53630](http://sedici.unlp.edu.ar/handle/10915/53630)

## <%= "#{resource.data.section_titles.awards.to_s}" %>
- I got the 3rd Award - Student / Systems category of the "Academic Excellence Award" of the Deloitte Foundation. Buenos Aires city - December 2012

