---
layout: page
title: Yet Another Posts List
locale_overrides:
  en:
    title: Yet Another Posts List
  es:
    title: Otra lista más de Posts
  pt:
    title: Mais uma lista de Posts
---
<% if site.locale == :en %>

**English**.
- So sorry, I do not have content in English yet

<% elsif site.locale == :es %>


<% elsif site.locale == :pt %>

**Português**.
- Desculpe, ainda não tenho conteúdo em português

<% end %>


Mi contenido en **Español**.
<ul>
  <% collections.posts.resources.each do |post| %>
    <% next if post.data.draft %>
    <li>
      <a href="<%= post.relative_url %>"><%= post.data.title %></a>
    </li>
  <% end %>
</ul>
