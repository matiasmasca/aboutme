---
layout: default
title: Home

locale_overrides:
  en:
    autor: Matias Mascazzini
    about:
        Workhorse Techie, Software Crafter. I like Ruby, Ruby On Rails and drink mate.
    description:
        Hello 👋, I'm **Matias**, a tech enthusiast (Techie) with curiosity and experience in various topics. You might remember me from a **talk** I've given, or from my years of co-organizing **tech events** for the community, or as a **technology consultant**, or even from my failed **ventures**. By now, I don't think it's because of my minimal skills in video games or playing the Spanish guitar.


        Today, I work as a **Software Engineer**, primarily with [Ruby](https://www.ruby-lang.org/) and [Ruby on Rails](https://rubyonrails.org/).


        If you're a techie, I’ll be sharing resources, my experiences, and some ideas that might interest you in the  **blog** and **talks** sections.


        I enjoy meeting new people; so whenever I visit a city and it’s possible, I'm open to chatting with other "techies" over coffee or mate. Feel free to reach out (if I don’t reply, feel free to follow up).


        If you’re looking **to work together** or with your client, you can find more information in the **work** & **bio** sections.


        This site is also a way for me to play around with [Bridgetown rb](https://www.bridgetownrb.com/), a website generator and a full-stack framework, powered by Ruby, of course.
    more_about: If you really want to know a little bit more about me please check the section
    language_disclaimer: I'm learning English as a second language, please let me know if there are any grammatical errors or typos. Thanks for be patient.
  es:
    autor: Matias Mascazzini
    about:
        Informatico todo terreno, artesano de software. Me gusta Ruby, Ruby On Rails y tomar Mate.
    description:
        Hola 👋, soy **Matias** un techie curioso con experiencia en diferentes de temas. Tal vez me recuerdes por alguna **charla** que he dado, o por mis años de co-organizar **eventos tecnologicos** para la comunidad, o como **consultor tecnologico**, o incluso por mis fallidos **emprendimientos**. A esta altura, no creo que me recuerdes por mis minimas habilidades en los video juegos o con la guitarra española.


        Hoy trabajo como **Software Engineer** principalmente con [Ruby](https://www.ruby-lang.org/) y [Ruby on Rails](https://rubyonrails.org/).


        Si eres **techie**, en este espacio, intentaré compartir recursos, mis experiencias y algunas ideas que te pueden llegar a interesar en la sección **blog** & **charlas**.


        Me gusta conocer nuevas personas; así que siempre que visito una ciudad y siempre que se pueda estoy disponible para charlar con otros "Techies", café o mate de por medio; sentite libre de contactarme (sino respondo insisti).


        Si me buscas **para trabajar** podras encontrar encontrar más informacion relacionada en las secciones **laboral** & **bios**.

        Este sitio, tambien es una forma de aprovechar para jugar con [Bridgetown rb](https://www.bridgetownrb.com/) un generador de sitios web y un framework full-stack potenciado, obviamente, con Ruby.
    more_about: Si realmente quieres saber un poco más sobre mi chequea la seccion
    language_disclaimer:
  pt:
    autor: Matias Mascazzini
    about:
        Cientista da computação todo-o-terreno, artesão de software. Eu gosto de Ruby, Ruby On Rails e beber Mate [padrão argentino ;) ].
    description:
        Olá 👋, sou **Matias**, um techie curioso com experiência em diferentes áreas. Talvez você se lembre de mim por alguma palestra que eu tenha dado, ou pelos meus anos co-organizando eventos tecnológicos para a comunidade, ou como consultor tecnológico, ou até mesmo pelos meus empreendimentos fracassados. A esta altura, acho que você não se lembrará de mim pelas minhas habilidades mínimas em videogames ou com o violão espanhol.


        Hoje trabalho como **Engenheiro de Software**, principalmente com [Ruby](https://www.ruby-lang.org/) e [Ruby on Rails](https://rubyonrails.org/).


        Se você é **techie**, aqui pretendo compartilhar recursos, minhas experiências e algumas ideias que podem te interessar na seção **blog** e **palestras**.


        Gosto de conhecer novas pessoas; então, sempre que visito uma cidade e é possível, estou disponível para conversar com outros **Techies**, com um café ou um chimarrão à disposição. Sinta-se à vontade para me contatar (se eu não responder, insista).


        Se você está me procurando para **trabalhar**, pode encontrar mais informações nas seções **trabalho** e **bios**.


        Este site também é uma forma de aproveitar para brincar com [Bridgetown rb](https://www.bridgetownrb.com/), um gerador de sites e um framework full-stack, claro, desenvolvido com Ruby.
    more_about: Se você quiser saber mais sobre mim, consulte a seção
    language_disclaimer: Meu português é bem básico, estou aprendendo como terceiro idioma com o Duolingo. Mas achei divertido e interessante começar a usar aqui no meu site. Eu realmente gostaria de praticar e começar a falar com alguma fluência. Entretanto, grande parte do conteúdo será traduzido com a ajuda de um tradutor e ChatGPT. Obrigado pela sua paciência e compreensão.
---

<h1>
    <div>
        <img class="selfi" src="<%= relative_url '/images/profile.jpeg' %>" alt="Avatar <%= site.metadata.title %>" />
    </div>
    <%= "#{resource.data.autor.to_s}" %>
</h1>

<div class="description">
    <%= "#{resource.data.about.to_s}" %>
</div>

<%= "#{resource.data.description.to_s}" %>
<div>
</div>
<div>
    <%= "#{resource.data.language_disclaimer.to_s}" %>
</div>
<div>
    <h2><%= "#{t("site.keep_in_touch")}" %></h2>
    <div class="flex-center-container">
        <%= render "link_tree" %>
    </div>
    <%= render "social_media" %>
</div>
<div>
</div>


