---
layout: page
title: Now
locale_overrides:
  en:
    title: What I’m doing Now?
    section_titles:
      work:
        title: Work
      community:
        title: Community Giveback
  es:
    title: ¿Qué estoy haciendo ahora?
    section_titles:
      work:
        title: Trabajo
      community:
        title: Devolver a la comunidad
  pt:
    title: o que estou fazendo agora?
    section_titles:
      work:
        title: Trabalho
      community:
        title: retribuição da comunidade
---

# <%= "#{resource.data.section_titles.work.title.to_s}" %>
<% if site.locale == :en %>
I’m currently working on the [Allyra](https://teamallegiance.com/allyra-online-fundraising-tool/) product (formerly WeDidIt), which is a web application that helps thousands of non-profit organizations across various sectors raise funds for their causes. As a software engineer, I focus on adding new features and keeping the existing ones updated in this complex application built with Ruby on Rails. I also handle monitoring and infrastructure tasks.


<% elsif site.locale == :es %>
Trabajando en el producto [Allyra](https://teamallegiance.com/allyra-online-fundraising-tool/) (anteriormente **WeDidIt**), que es una aplicación web que ayuda a miles de organizaciones sin fines de lucro, en diferentes verticales, a recaudar dinero para utilizar en sus causas. Como **Software engineer**, me enfoco en agregar nuevas funcionalidades y también en mantener actualizadas las antiguas de esta compleja aplicación hecha con [Ruby on Rails](https://rubyonrails.org/). Además de tareas de monitoreo y infraestructura.

<% elsif site.locale == :pt %>
Atualmente, estou trabalhando no produto [Allyra](https://teamallegiance.com/allyra-online-fundraising-tool/) (anteriormente WeDidIt), que é uma aplicação web que ajuda milhares de organizações sem fins lucrativos, em diferentes áreas, a arrecadar fundos para suas causas. Como engenheiro de software, meu foco é adicionar novas funcionalidades e também manter as antigas atualizadas nesta complexa aplicação desenvolvida com [Ruby on Rails](https://rubyonrails.org/). Além disso, realizo tarefas de monitoramento e infraestrutura.

<% end %>

#  <%= "#{resource.data.section_titles.community.title.to_s}" %>
<% if site.locale == :en %>
In 2024, I started collaborating as a Teaching Assistant at the local public university [FaCENA - UNNE](https://exa.unne.edu.ar/), for the courses "Paradigms and Languages" and "Object-Oriented Programming."

I’m not actively organizing activities for tech communities like I used to, but you’ll probably still find me participating. From time to time, I try to prepare a talk and participate in tech events.

<% elsif site.locale == :es %>
En 2024, comencé a colaborar como Profesor Asistente en la universidad pública local [FaCENA - UNNE](https://exa.unne.edu.ar/), para la materia "paradigmas y lenguajes" y para la materia "Programación orientada a objetos".

No estoy organizando activamente actividades para las comunidades tecnológicas como lo solía hacer, pero probablemente aún podrás encontrar participando. De vez en cuando, intento preparar una charla y participar en eventos tecnológicos.

<% elsif site.locale == :pt %>
Em 2024, comecei a colaborar como Professor Assistente na universidade pública local [FaCENA - UNNE](https://exa.unne.edu.ar/), nas disciplinas "paradigmas e linguagens" e "Programação Orientada a Objetos".

Não estou organizando ativamente atividades para as comunidades tecnológicas como costumava fazer, mas provavelmente você ainda me encontrará participando. De vez em quando, tento preparar uma palestra e participar de eventos tecnológicos.

<% end %>