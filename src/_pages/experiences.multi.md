---
layout: projects
title: Yet Another Work Experience List
locale_overrides:
  en:
    title: Yet Another Work Experience List
    section_titles:
      tl_dr:
        title: 'TL;DR'
        items:
          item_1: I'm a Web Developer with wide experience
          item_2: I work remotely
          item_3: For a while, I was a developer freelance, a tech consultant, and a tech entrepreneur
      looking_for:
        title: What I am looking for?
        items:
          item_1: Remote work
          item_2: Be part of a real Agile Team, with SR, SSR, and Jr. members
          item_3: Ruby projects or similar modern technologies.
          item_4: Long-lasting relationships. Long-term contract.
          item_5: I prefer Product based over Services companies.
          item_6: A company culture that supports non-native speakers and diversity.
        pos_paragraph: One of my biggest weak points, right now, is my conversational English skill but if you have the patience and the culture to support not native speakers, I'll do my best. Meanwhile, sometimes I might need some rephrase or writing support.
      featured_experience:
        title: FEATURED JOB EXPERIENCE
      job_experience: JOB EXPERIENCES
  es:
    title: Otra lista más de EXPERIENCIA LABORAL
    section_titles:
      tl_dr:
        title: Resumen Ejecutivo
        items:
          item_1: Soy Desarrollador Web con amplia experiencia.
          item_2: Trabajo de forma remota
          item_3: Durante un tiempo, fui desarrollador independiente, consultor tecnológico y emprendedor tecnológico.
      looking_for:
        title: ¿Qué estoy buscando?
        items:
          item_1: Trabajo remoto
          item_2: Se parte de un equipo ágil, con miembros de diferentes niveles de experiencia (SR, SSR, y Jr.)
          item_3: Proyectos web, con Ruby o con tecnologías modernas con valor para la industria.
          item_4: Relaciones duraderas. Contrato a largo plazo.
          item_5: Prefiero las empresas basadas en productos a las de servicios.
          item_6: Una cultura empresarial que apoya a los hablantes no nativos y la diversidad.
        pos_paragraph: Mi mayor debilidad es el inglés a nivel conversacional, aunque estoy trabajando en ello, si la empresa tiene la cultura de ser pacientes (o adaptación del perfil al idioma) hablemos, esto para mi es una buena señal.
      featured_experience:
        title: EXPERIENCIAS DESTACADAS
      job_experience: EXPERIENCIA LABORAL
  pt:
    title: Mais uma lista de EXPERIÊNCIAS DE TRABALHO
    section_titles:
      tl_dr:
        title: 'Sumário executivo'
        items:
          item_1: Sou um Desenvolvedor Web com ampla experiência
          item_2: Eu trabalho remotamente em casa
          item_3: Por um tempo, fui desenvolvedor freelancer, consultor de tecnologia e empreendedor de tecnologia
      looking_for:
        title: O que estou procurando?
        items:
          item_1: Trabalho remoto
          item_2: Quero fazer parte de um time ágil de verdade, com membros Sr, Semi Sr e Jr.
          item_3: Projetos Ruby ou tecnologias modernas similares com valor para a indústria
          item_4: Relação de longa duração. Contrato de longa duração.
          item_5: Uma cultura de empresa que apoia falantes não nativos e diversidade.
          item_6: Prefiro empresas baseadas em produtos a empresas de serviços.
        pos_paragraph: Um dos meus maiores pontos fracos, no momento, é minha habilidade de conversação em inglês, mas se você tiver paciência e cultura para apoiar falantes não nativos, farei o meu melhor. Enquanto isso, às vezes posso precisar de alguma reformulação ou suporte de redação. Não falo português fluentemente, conheço apenas algumas palavras e verbos, mas adoraria praticar com falantes nativos.
      featured_experience:
        title: EXPERIÊNCIA DE TRABALHO EM DESTAQUE
      job_experience: EXPERIÊNCIAS DE TRABALHO


---
# <%= "#{resource.data.section_titles.tl_dr.title.to_s}" %>
- <%= "#{resource.data.section_titles.tl_dr.items.item_1.to_s}" %>
- <%= "#{resource.data.section_titles.tl_dr.items.item_2.to_s}" %>
- <%= "#{resource.data.section_titles.tl_dr.items.item_3.to_s}" %>

# <%= "#{resource.data.section_titles.looking_for.title.to_s}" %>
1. <%= "#{resource.data.section_titles.looking_for.items.item_1.to_s}" %>
2. <%= "#{resource.data.section_titles.looking_for.items.item_2.to_s}" %>
3. <%= "#{resource.data.section_titles.looking_for.items.item_3.to_s}" %>
4. <%= "#{resource.data.section_titles.looking_for.items.item_4.to_s}" %>
5. <%= "#{resource.data.section_titles.looking_for.items.item_5.to_s}" %>
6. <%= "#{resource.data.section_titles.looking_for.items.item_6.to_s}" %>

<%= "#{resource.data.section_titles.looking_for.pos_paragraph.to_s}" %>

# <%= "#{resource.data.section_titles.featured_experience.title.to_s}" %>
## Web Development Engineer
At [Allegiance Fundraising](https://teamallegiance.com/)  - Fargo, ND, USA. **REMOTE**\
Oct 2020 - Present

I work remotely at the Technology Solutions Division on the product [Allyra / WeDidIt platform](https://wedid.it) with the Ruby on Rails tech stack. I did tasks of maintenance and develop new features.

- I upgrade the AWS infraestructure.
- I upgraded the Rails version.
- I helped to stop a DDoS Attack.
- I documented the infrastructure and procedures that were unknown to the whole team.
- I trained the new members (including Jr level) and support them through the whole process to become active members.
- I won the **Allegiance Group Momentum Award** in 2021 & 2022, and the **Allegiance Group ABOVE & BEYOND Award** in 2023. Thanks to my team mates for the nominations.


## Full Stack Web Developer
At [Terciar Consultores](https://www.linkedin.com/company/terciar/)   – Corrientes, ARG. **REMOTE**\
May 2017– Ago 2020
- **Technical leader** and developer for custom software development with Ruby on Rails stack. Usually with one or two tech collaborators.
- **Technical responsible** for most of the projects for different industries like communication, education, professional services, retail, and government.

## **Full Stack Web eveloper**
At [BFA.com](https://BFA.com) - NYC, NY, USA. **REMOTE**\
Jan 2016 - Feb 2017
- I collaborated in the maintenance of the web application used for booking photography services with Rails & React.
- I collaborated with the Front-end development (html, css, ClojureScript) and backend (Clojure) for a spin-off of them.
- I did some proofs of concept for integration with third-party APIs, which would then be integrated into the company's main application.

# <%= "#{resource.data.section_titles.job_experience.to_s}" %>
<% collections.jobs.resources.each do |job| %>
  <% next if job.data.draft %>
## **<%= job.data.position_title %>**
<%= job.data.start_date %> – <%= job.data.end_date %>. <%= job.data.company_name %>. (<%= job.data.office_type %>).
<%= job.data.place %>.

<%= job.data.job_description %>

<%= "- #{job.data.achievement_1}" if job.data.achievement_1 %>
<%= "- #{job.data.achievement_2}" if job.data.achievement_2 %>
<%= "- #{job.data.achievement_3}" if job.data.achievement_3 %>
<%= "- #{job.data.achievement_4}" if job.data.achievement_4 %>
<%= "- #{job.data.achievement_5}" if job.data.achievement_5 %>
<%= "- #{job.data.achievement_6}" if job.data.achievement_6 %>

<% if job.data.view_more %>
<a href="<%= job.relative_url %>">View more</a>
<% end %>

<% end %>

If you want to reach me and you read all this text tell me the secret keywords: "mates from Gitlab site"