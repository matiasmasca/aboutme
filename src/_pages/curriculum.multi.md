---
layout: page
title: Yet Another Resume
resume:
  url: https://drive.google.com/file/d/1Um1W-uLU6cuJ0YQBKO_8cO577lQ34sAS/view?usp=sharing
locale_overrides:
  en:
    title: Yet Another Resumen
  es:
    title: Un currículum vitae más
  pt:
    title: Mais um Curriculum vitae
---

<% if site.locale == :en %>

You can check my classic resume or Curriculum vitae

<a href="<%= site.metadata.resume.url %>">Download it </a>

<% elsif site.locale == :es %>

Por el momento mi currículum solo esta disponible en inglés

<a href="<%= site.metadata.resume.url %>">Descargar</a>

<% elsif site.locale == :pt %>

No momento, meu currículo só está disponível em inglês

<a href="<%= site.metadata.resume.url %>">Baixe </a>
<% end %>



<iframe src="https://drive.google.com/file/d/1Um1W-uLU6cuJ0YQBKO_8cO577lQ34sAS/preview" width="100%" height="1080" allow="autoplay"></iframe>

