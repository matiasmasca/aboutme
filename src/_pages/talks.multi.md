---
layout: page
title: Yet Another Talks List
locale: multi
locale_overrides:
  en:
    title: Yet Another Talks List
    community_organizer_title: COMMUNITY CONFERENCES, SEMINARS OR WORKSHOP as ORGANIZER
    community_organizer_description:
      I collaborated in different ways with the organization of the followings community events; always working in teams with other volunteers
    community_talks_title: COMMUNITY EVENTS, CONFERENCES, SEMINARS AS Speaker
    other_talks_title: OTHER CONFERENCES, SEMINARS, TALKS AS Speaker
    courses_title: COURSES OR WORKSHOP AS Speaker
    courses: Courses
    workshops: Workshops
    course: Course
    workshop: Workshop
    event: Event
  es:
    title: Otra lista más de Charlas
    community_organizer_title: CONFERENCIAS, SEMINARIOS, TALLERES COMUNITARIOS como ORGANIZADOR
    community_organizer_description:
      Colaboré de diferentes maneras con la organización de los siguientes eventos comunitarios; siempre trabajando en equipo con otros voluntarios
    community_talks_title: CONFERENCIAS Y EVENTOS COMUNITARIOS como Disertante
    other_talks_title: OTRAS CONFERENCIAS, SEMINARIS Y CHARLAS como Disertante
    courses_title: CURSOS Y TALLERES como Disertante
    courses: Cursos
    workshops: Talleres
    course: Curso
    workshop: Taller
    event: Evento
  pt:
    title: Outra lista de palestras
    community_organizer_title: CONFERÊNCIAS COMUNITÁRIAS, SEMINÁRIOS OU WORKSHOP como ORGANIZADOR
    community_organizer_description:
      Colaborei de diversas formas com a organização dos seguintes eventos comunitários; sempre trabalhando em equipe com outros voluntários
    community_talks_title: CONFERÊNCIAS E EVENTOS COMUNITÁRIOS como Palestrante
    other_talks_title: OUTRAS CONFERÊNCIAS, SEMINÁRIOS, PALESTRAS como Palestrante
    courses_title: COURSES OR WORKSHOP AS Speaker
    courses: Cursos
    workshops: workshops
    course: Curso
    workshop: Workshop
    event:  Evento

---

## <%= "#{resource.data.community_talks_title.to_s}" %>
<ul>
  <% collections.talks.resources.each do |talk| %>
    <% next if talk.data.draft %>
    <% next unless talk.data.community %>
    <li>
      <a href='<%= talk.relative_url %>'><%= "#{talk.data&.date.year}: #{talk.data&.event_name} - #{talk.data.title}" %></a>
    </li>
  <% end %>
</ul>

<div class='gallery'>
  <img src='<%= relative_url '/images/talks/saso_2017_group.JPG' %>' title='SASO 2017'>
  <img src='<%= relative_url '/images/talks/saso_2017_DSCN8194.JPG' %>' title='SASO 2017 Speaker'>
  <img src='<%= relative_url '/images/talks/saso_speaker_2022.png' %>' title='SASO 2022 Speaker'>
  <img src='<%= relative_url '/images/talks/flisol_16_DSC_0129.JPG' %>' title='FLISOL 2016 Resistencia'>
  <img src='<%= relative_url '/images/talks/flisol16_DSC_0120.JPG' %>' title='FLISOL 2016 Resistencia'>
  <img src='<%= relative_url '/images/talks/2022_Nedearla_101_v2.png' %>' title='Nerdearla 101 v2'>
</div>

## <%= "#{resource.data.other_talks_title.to_s}" %>
<ul>
  <% collections.talks.resources.each do |talk| %>
    <% next if talk.data.draft %>
    <% next if talk.data.community %>
    <li>
      <a href='<%= talk.relative_url %>'><%= "#{talk.data&.date.year}: #{talk.data&.event_name} - #{talk.data.title} " %></a>
    </li>
  <% end %>
</ul>
<br>
<br>

<div class='gallery'>
  <img src='<%= relative_url '/images/talks/joinea_14567599_685724844913165_864053864652569062_o.jpg' %>' title='JoiNEA 2016'>
  <img src='<%= relative_url '/images/talks/joinea_14524536_685724581579858_387116932678649962_o.jpg' %>' title='JoiNEA 2016'>
  <img src='<%= relative_url '/images/talks/20150903_42jaiio.jpg' %>' title='Poster 42 JAIIO EST'>
  <img src='<%= relative_url '/images/talks/20180517_agile_inception_workshop_ucp_1.jpg' %>' title='UPC Expert 2018'>
  <img src='<%= relative_url '/images/talks/20190516_ucp_inception_desk.jpg' %>' title='UPC Expert 2019'>
  <img src='<%= relative_url '/images/talks/20180517_agile_inception_workshop_ucp.jpg' %>' title='Agile Product Box'>
</div>

## <%= "#{resource.data.courses_title.to_s}" %>
### <%= "#{resource.data.courses.to_s}" %>
- **<%= "#{resource.data.course.to_s}" %>**: Python 3 Instructor – Professorship: “Programming 1: introduction to programming
with Python” for a Associate Degree Course
  - From March – July 2020.
  - Universidad de la Cuenca del Plata.
  - Corrientes city, Argentina.
  - Tech stack: Python v3 + PyGame
  - [Course materials (Spanish)](https://github.com/matiasmasca/curso_python)
 - **<%= "#{resource.data.course.to_s}" %>**: Ruby On Rails Instructor – Course “Web application development with Ruby On
Rails”
   - From May – July 2018.
	 - Universidad Nacional del Nordeste
	 - Corrientes city, Argentina.
 - **<%= "#{resource.data.course.to_s}" %>**: Gov Program 111mil - JAVA Instructor
   - From May - December 2017.
	 - Universidad Nacional del Nordeste
	 - Corrientes city, Argentina.
   - Tech stack: Agile + Java desktop
   - [Course materials (Spanish)](https://github.com/111milprogramadores)

### <%= "#{resource.data.workshops.to_s}" %>
- **<%= "#{resource.data.workshop.to_s}" %>**: VIII JoINEA – Workshop “Introducción al versionado de código con GIT”. 23 & 24 September 2016 Apostoles city, Argentina
- **<%= "#{resource.data.workshop.to_s}" %>**: "GIT... es por amor". 10th August 2016. FACENA – UNNE. Corrientes city, Argentina
- **<%= "#{resource.data.workshop.to_s}" %>**: "GIT es nuestro amigo v2". 6th August 2016. Informatorio. Presidencia Roque Sáenz Peña city, Argentina
- **<%= "#{resource.data.workshop.to_s}" %>**:  "GIT es nuestro amigo"". 30 July 2016. Informatorio. Resistencia city, Argentina
<br>
<br>
<div class='gallery'>
  <img src='<%= relative_url '/images/talks/2016_git_informatorio.jpg' %>' title='Git Workshop 2016 Resistencia'>
  <img src='<%= relative_url '/images/talks/2016_sp_1DSC_0066.JPG' %>' title='Git Workshop 2016 Informatorio Chaco'>
  <img src='<%= relative_url '/images/talks/2017_111mil_curso_java.jpeg' %>' title='Curso 111mil Programadores'>
</div>

## <%= "#{resource.data.community_organizer_title.to_s}" %>
*<%= "#{resource.data.community_organizer_description.to_s}" %>*
 - **<%= "#{resource.data.event.to_s}" %>**: Global Day of Coderetreat Argentina.
	 - Host and facilitator for editions: 2013, 2014, 2015, 2016, 2017 2018, 2019.
	 - Corrientes and Resistencia cities, Argentina.
	 - [http://coderetreat.org](http://coderetreat.org)
 - **<%= "#{resource.data.event.to_s}" %>**: BarCamp NEA.
	 - Editions 2012, 2013, 2014, 2015, 2016 y 2017. Corrientes,
	 - Resistencia, Formosa and Apóstoles cities, Argentina.
	 - [https://www.facebook.com/barcampnea](https://www.facebook.com/barcampnea)
 - **<%= "#{resource.data.event.to_s}" %>**: Hackathon Express.
	 - Editions 2012, 2013, 2014 y 2015.
	 - Corrientes and Resistencia cities, Argentina.
	 - [https://www.facebook.com/hackathonexpress/](https://www.facebook.com/hackathonexpress/)
 - **<%= "#{resource.data.event.to_s}" %>**: Arduino Global Day
	 - Editions 2015, 2016, 2017.
	 - Corrientes and Resistencia cities, Argentina.
	 - [https://day.arduino.cc/](https://day.arduino.cc/)
 - **<%= "#{resource.data.event.to_s}" %>**: Corrientes I/O
   - Google I/O Extend GDG Community Event
   - Edition 2013
 - **<%= "#{resource.data.event.to_s}" %>**: Rayos y Centellas, Google!
   - Edition 2012
   - By GDG Corrientes & ComunidadTIC
 - **<%= "#{resource.data.event.to_s}" %>**: Mate Camp
   - Editions 2010, 2011, 2012.
   - Corrientes city, Argentina.
   - Meetup event
- **<%= "#{resource.data.event.to_s}" %>**: Software Freedom Day
   - Edition 2008
   -  Resistencia city, Argentina.
   - [https://www.softwarefreedomday.org/](https://www.softwarefreedomday.org/)


<div class='gallery'>
  <img src='<%= relative_url '/images/events/BarcampNEA_2011.jpg' %>' title='BarcampNEA 2011'>
  <img src='<%= relative_url '/images/events/barcamp 2012 corrientes.jpeg' %>' title='BarcampNEA 2012'>
  <img src='<%= relative_url '/images/events/barcampnea2013.jpg' %>' title='BarcampNEA 2013'>

  <img src='<%= relative_url '/images/events/barcamp_2015_resi.jpeg' %>' title='BarcampNEA 2015'>
  <img src='<%= relative_url '/images/events/barcamp2016_talk.jpg' %>' title='BarcampNEA 2016 Formosa talk'>
  <img src='<%= relative_url '/images/events/barcamp2016.jpeg' %>' title='BarcampNEA 2016 Formosa'>
  <img src='<%= relative_url '/images/events/ArduionDay2017.jpeg' %>' title='Arduino Day 2017'>
  <img src='<%= relative_url '/images/events/gdcr_2015_1114_174102.jpg' %>' title='Global Day of Coderetreat 2015'>
  <img src='<%= relative_url '/images/events/gdcr_2015_1114_175428.jpg' %>' title='Global Day of Coderetreat 2015'>
  <img src='<%= relative_url '/images/events/gdcr_2017.jpg' %>' title='Global Day of Coderetreat 2017'>
</div>