---
layout: projects
title: Yet Another Project List
locale_overrides:
  en:
    title: Yet Another Project List
  es:
    title: Otra lista más de Proyectos
  pt:
    title: Mais uma lista de Projetos
---
<% if site.locale == :en %>

<% elsif site.locale == :es %>

<% elsif site.locale == :pt %>

<% end %>

<ul>
  <% in_locale(collections.projects.resources).each do |project| %>
    <% next if project.data.draft %>
    <li>
      <a href="<%= project.relative_url %>"><%= project.data.title %></a>
    </li>
  <% end %>
</ul>
