{
  mastodon: 'https://ruby.social/@matiasmasca',
  twitter: 'https://twitter.com/matiasmasca',
  linkedin: 'https://www.linkedin.com/in/matiasmasca',
  youtube: 'https://youtube.com/channel/UClULwcTBLsvmcBBuyMREKmg',
  medium: 'https://medium.com/@matiasmasca',
  gitlab: 'https://gitlab.com/matiasmasca',
  github: 'https://github.com/matiasmasca',
  stackoverflow: 'https://stackoverflow.com/users/2191500/matiasmasca',
  contact: 'matiasmasca+blog@gmail.com'
}
