---
title: "Desarrollo de una aplicación web
utilizando Desarrollo Guiado por Comportamiento e Integración Continua"
layout: talks
date: '2015-09-4 17:00:00 -0300'
place:  Rosario city, Argentina.
category: conf
community: false
event_name: 44o JAIIO
event_url: https://44jaiio.sadio.org.ar/
description: my experience developing a web app with Test-Driven Development (TDD), Behavior-Driven Development (BDD) and Continuous Integration practices and tools.
externalLink: http://sedici.unlp.edu.ar/handle/10915/59900
---

**Event name**: *<%= resource.data.event_name %>* -  [Official Event site(<%= resource.data.event_url %>)

I spoken about <%= resource.data.description %>

Symposium EST.
Paper title: “Desarrollo de una aplicación web
utilizando Desarrollo Guiado por Comportamiento e Integración Continua” (pag. 72-93)
ISSN: 2451-7615.

Paper available on [Follow link](<%= resource.data.externalLink %>).
