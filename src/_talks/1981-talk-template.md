---
title: "Talk name"
layout: talks
date: '2022-12-09 12:00:00 -0300'
place: city, country
draft: true
category: conf
community: true
event_name: Conf
event_url: https://www.duckduckgo.com/
description: Here a description about the talk
externalLink: https://www.duckduckgo.com/
---

**Event name**: *<%= resource.data.event_name %>* [Event Site](<%= resource.data.event_url %>)

I spoken about <%= resource.data.description %>


Slides available on [Follow link](<%= resource.data.externalLink %>).
