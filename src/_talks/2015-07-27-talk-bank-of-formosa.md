---
title: "BDD (Behavior-Driven Development) & CI (Continuous Integration)"
layout: talks
date: '2015-07-26 14:00:00 -0300'
place:  Formosa city, Argentina.
category: course
draft: false
community: false
event_name: Seminar
event_url: https://www.bancoformosa.com.ar/
description: my experience developing a web app with Test-Driven Development (TDD), Behavior-Driven Development (BDD) and Continuous Integration practices and tools. I was invited to talk as part of a post-grade course for the IT department of the bank "Banco de Formosa".
externalLink:
---

**Event name**: *<%= resource.data.event_name %>* [Event Site](<%= resource.data.event_url %>)

I spoken about <%= resource.data.description %>



