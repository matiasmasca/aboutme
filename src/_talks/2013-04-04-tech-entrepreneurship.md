---
title: "Emprendedurismo Tecnológico"
layout: talks
date: '2013-04-04 12:00:00 -0300'
place: Corrientes city, Argentina
draft: false
category: talks
community: false
event_name: "UNNE"
event_url: https://exa.unne.edu.ar
description: my experience as a tech entrepreneur in Corrientes city, Argentina, with the intention of spread the ideas about tech entrepreneurship as an alternative to find a job in the government or for the local industry of that time.
externalLink:
---

**Event name**: *<%= resource.data.event_name %>* [Event Site](<%= resource.data.event_url %>)

I spoken about <%= resource.data.description %>