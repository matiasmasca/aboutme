---
title: "Introducción a la agilidad en proyectos con Scrum"
layout: talks
date: '2017-05-26 15:00:00 -0300'
category: conf
community: true
event_name: SASO Conf 2017
event_url: https://sasoconf.ar/
description:
externalLink:
---

**Event name**: *<%= resource.data.event_name %>* \
[Event Site](<%= resource.data.event_url %>)

I spoken about the Agile movement and how we use Scrum on our daily basis

I wrote a blog post about sharing my experience traveling to participate on this event as speaker [in Spanish](https://medium.com/memorias-de-un-techie/sasoconf-2017-cronicas-3cef2d0fb772)

<% if resource.data.externalLink %>
  Slides available on [Follow link](<%= resource.data.externalLink %>).
<% end %>
