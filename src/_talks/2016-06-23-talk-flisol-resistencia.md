---
title: "Desarrollo de software dirigido
por comportamiento & integración continua: Otra forma de hacer software"
layout: talks
date: '2016-06-23 15:00:00 -0300'
place:  Resistencia city, Argentina.
category: conf
community: true
event_name: FLISOL 2016 Resistencia city
event_url: https://flisol.info/FLISOL2016/Argentina/Resistencia
description: my experience developing a web app with Test-Driven Development (TDD), Behavior-Driven Development (BDD) and Continuous Integration practices and tools.
externalLink:
---

**Event name**: *<%= resource.data.event_name %>* \
[Event Site](<%= resource.data.event_url %>)

I spoken about <%= resource.data.description %>


<% if resource.data.externalLink %>
  Slides available on [Follow link](<%= resource.data.externalLink %>).
<% end %>
