---
title: "Agile Inception Desk"
layout: talks
date: '2018-05-18 10:00:00 -0300'
draft: false
category: seminar
community: false
event_name: "UCP Experts' visit"
event_url: https://www.ucp.edu.ar
description: the Agile Inception Desk and how we can uses in the initial phases of a project.
externalLink:
---

**Event name**: *<%= resource.data.event_name %>* [Event Site](<%= resource.data.event_url %>)

I was invited to speck in a university course as an "Industry Expert". I spoken about <%= resource.data.description %>

Thank you Professor [Gilda Romero](https://twitter.com/gil2rom) for the invitation.