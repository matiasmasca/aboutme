---
title: "¿Cuánto cobrar por tu trabajo? Armemos tu calculadora Freelo"
layout: talks
date: '2022-08-05 10:00:00 -0300'
category: conf
community: true
event_name: Nedearla 101 v2
event_url: https://nerdear.la/
description: It was a web app for the administration of communication with clients
  in professional studio
externalLink: https://bit.ly/calculadoranerdearla
---

**codename**: *Nedearla 101 v2*

I spoken about how to calculate in a better way your rates when you are a Freelancer. It was a virtual conference.

<iframe width="560" height="315" src="https://www.youtube.com/embed/tLTK8Zh_SJE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Slides available on [Follow link](<%= resource.data.externalLink %>).