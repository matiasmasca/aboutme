---
title: "Desarrollo de una aplicación web utilizando BDD & C-I"
layout: talks
date: '2015-12-5 14:00:00 -0300'
place:  Rosario city, Argentina.
category: conf
community: true
event_name: ágiles argentina 2015
event_url:  http://aa2015.agiles.org
description: my experience developing a web app with Test-Driven Development (TDD), Behavior-Driven Development (BDD) and Continuous Integration practices and tools.
externalLink: https://medium.com/memorias-de-un-techie/cr%C3%B3nicas-de-%C3%A1giles-argentina-2015-abc9b3815b6c
---

**Event name**: *<%= resource.data.event_name %>* \
[Event Site](<%= resource.data.event_url %>)

It was an Open Space event about Agile in general. I spoken about <%= resource.data.description %>

I wrote a blog post about sharing my experience traveling to participate on this event as speaker [in Spanish](<%= resource.data.externalLink %>)