---
title: "Experiencia en recuperación de alumnos que adeudan el TFA en la Licenciatura en Sistemas de Información de la UNNE"
layout: talks
date: '2016-06-01 16:00:00 -0300'
place:  Morón city, Argentina.
category: conf
community: false
event_name: XI Congreso TE&ET
event_url: https://teyet2016.unimoron.edu.ar/
description: how we helped university students to complete their degrees. Guide them to complete the final degree project
externalLink: http://sedici.unlp.edu.ar/handle/10915/53630
---

**Event name**: *<%= resource.data.event_name %>* -  [Official Event site(<%= resource.data.event_url %>)

I spoken about <%= resource.data.description %>

Event: XI Congreso de Tecnología en Educación y Educación en Tecnología (TE&ET 2016).
Paper title: “Experiencia en recuperación de alumnos que adeudan el TFA en la Licenciatura en Sistemas de Información de la UNNE”
ISBN: 978-987-3977-30-5
Pages: 204-209

Paper available on [Follow link](<%= resource.data.externalLink %>).