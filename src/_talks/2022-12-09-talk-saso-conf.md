---
title: "Cuánto cobrar por tu trabajo?"
layout: talks
date: '2022-12-09 12:00:00 -0300'
category: conf
community: true
event_name: SASO Conf 2022
event_url: https://sasoconf.ar/
description: It was a web app for the administration of communication with clients
  in professional studio
externalLink: https://docs.google.com/spreadsheets/d/1BPJo0AQKZCujdHYgCiF1tTnjk1KJrAwOdJADDf3Q9_c/edit#gid=585384008
---

**Event name**: *<%= resource.data.event_name %>* \
[Go to Event Site](<%= resource.data.event_url %>)

I spoken about how to calculate in a better way your rates when you are a Freelancer. It was a on site conference.

Slides available on [Follow link](<%= resource.data.externalLink %>).