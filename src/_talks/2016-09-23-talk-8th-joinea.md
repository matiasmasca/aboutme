---
title: "Desarrollo de una aplicación web utilizando Desarrollo Guiado por Comportamiento (BDD) e Integración Continua (C-I)"
layout: talks
date: '2016-09-23 15:00:00 -0300'
place:  Apostoles city, Argentina.
category: conf
community: false
event_name: VIII JoINEA
event_url: http://www.joinea.org/
description: my experience developing a web app with Test-Driven Development (TDD), Behavior-Driven Development (BDD) and Continuous Integration practices and tools.
externalLink:
---

**Event name**: Jornadas de Integración, Extensión y Actualización de Estudiantes de Informática. *<%= resource.data.event_name %>* [Event Site](<%= resource.data.event_url %>)

I spoken about <%= resource.data.description %>

<iframe width="560" height="315" src="https://www.youtube.com/embed/jGNBqKkSIZg?start=7384" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
