---
layout: post
title: '¿Cómo se puede cobrar por tu trabajo?'
date: 2020-09-21 22:00:00 -0300
categories: blog
locale: es
image: "/assets/images/markdown.jpg"
headerImage: false
tag:
- Costo
- Freelance
- Freelancers
- Hourly Rate
- Hours Of Service
author: matiasmasca
description: Reflexión sobre diferentes maneras de cobrar por un trabajo realizado con foco en el trabajo independiente
---

# **¿Cómo se puede cobrar por tu trabajo? Reflexiones de un emprendedor**

**_Existen diferentes maneras de cobrar por el trabajo las que generalmente se realizan en base al factor común: tiempo. Conocer esto puede ayudarte a mejorar el manejo de tu economía y a no cometer errores muy comunes al poner a trabajar tu conocimiento al servicio de otros._**

![](https://miro.medium.com/max/700/1*uPrhaWkjKrlXWv9686Zhqg.png)

Fuente: Wikimedia — Author: [Rogerborrell](https://commons.wikimedia.org/w/index.php?title=User:Rogerborrell&action=edit&redlink=1)

Al empezar la vida laboral una de las primeras interrogantes que surgen es **¿Cómo cobrar**? y **¿Cuánto cobrar**? independientemente si es al elegir un empleo como al realizar un trabajo para un cliente; no importa si eres autodidacta, artesano o graduado siempre en algún momento tenemos que contestar esta pregunta. En estos textos voy a tratar de comentarte algunas de las formas que conozco de cómo se cobra y dejaré para otro momento el cuanto cobrar.

En este planeta sus habitantes tenemos de igual manera las misma **24 horas** del día en los mismos 365 días del año, la cuestión clave es entonces **¿Cómo sacarle el mayor provecho económico posible**? y de eso quiero reflexionar estos textos.

Comúnmente existen 2 formas aceptadas de percibir una remuneración por nuestro trabajo una es **por tiempo**, en el cual se establece una cantidad fija de dinero por una cantidad fija de tiempo (jornal, semanal, quincenal, mensual, etc) y la otra es **por trabajo terminado** o ‘trabajo a destajo’ donde se recibe una cantidad fija por una cantidad fija de trabajo realizado, sin importar el tiempo en el que se incurra.

Y también 2 maneras legales generales que definen tu relación comercial con el que solicita tu trabajo, una en **“Relación de dependencia**”, cuando pones tu fuerza de trabajo a disposición de una organización, y la otra “**de forma Independiente**”. Estas dos condiciones, al igual que cualquier acuerdo entre personas, traen sus múltiples combinaciones y alternativas posibles.

# 1. Trabajadores en relación de dependencia.

![](https://miro.medium.com/max/700/1*KqrZLzGfY2TP-dH7uvb1yw.jpeg)

Fig.1: Remuneración para Empleados

Cuando eres empleado podemos decir que a cambio de poner tu fuerza de trabajo a disposición del empleador recibes una remuneración, de qué manera y cada cuánto son cuestiones fundamentales para un empleado.

Cuando lo único que importa es el **tiempo**, tu ciclo económico será generalmente ir a trabajar en un lugar una cierta cantidad de horas y a cierta altura del mes cobrar tu salario; incluso seguirás percibiendo este ingreso aunque no vayas a trabajar (licencias, vacaciones pagas, etc.) entre otros beneficios. Así hay sectores o actividades que pagan por día o jornada de trabajo, por quincena e incluso por temporada.

Cuando lo más importante es el **rendimiento**, existen otras formas de cobrar llamadas **“trabajo a destajo**”, que paga por unidad producida o por tarea realizada. Por ejemplo alguien que levanta a mano una cosecha generalmente se le paga por canasta cosechada no importa cuánto tiempo le llevó hacerlo; o una encuestadora a la que se le paga por encuesta producida . Otra forma es la de recibir una comisión

Hasta aquí es camino simple, solo tienen que saber si lo que van a pagar de bolsillo te sirve para sostener o mejorar tu estilo de vida y qué otros beneficios te ofrecen y cuales serán tus responsabilidades.

# 2. Trabajadores independientes.

Ahora vayamos por el otro camino, el del trabajo en forma independiente y su forma de cobrar, que es lo que despierta más dudas e interrogantes, cuando te llegan esas primeras ofertas y reflexionemos sobre distintas formas comerciales de cobrar conceptualmente por ese trabajo.

Podemos decir que el trabajo independiente se realiza **por proyectos o por la prestación de un servicio** y que generará algo como resultado que la otra parte, el cliente, está dispuesto a pagar. En ciertos casos para realizar su trabajo el profesional independiente necesitará de insumos y en otros lo más intensamente intelectuales no los necesitará tanto.

Mi años de emprendedor y profesional independiente de sistemas, me hicieron conocer **3 formas principales** por las cuales podemos cobrar por un trabajo de forma independiente: **por el trabajo realizado, por el tiempo consumido y por el valor generado**, esta última es la más sofisticada y con mayor potencial de generar grandes ganancia a un negocio.

![](https://miro.medium.com/max/700/1*nxy8nfwrzrnSP_VBtsSpAA.jpeg)

Fig. 2: Posibles formas de cobrar para Profesionales Independientes

# **Por trabajo**

Por trabajo realizado, es el más común de los casos **“-¿Cuánto me sale que hagas tal cosa?”**, donde tal cosa puede ser construir una pared, cortar el pelo, hacer un programa de computadora o confeccionar una escritura para una casa, no importa lo que sea al “Cliente” lo primero que le interesa es el resultado final de tu trabajo y el costo total que tendrá para él, las razones del porqué quiere o necesita que se haga no son siempre bien conocidas y generalmente deberás indagar al respecto.

**2.1.1 Presupuesto cerrado**

(también conocido en inglés como **Fixed Price**)

Generalmente cuando tu primer respuesta sea $ XX.XXX monto, consciente o inconscientemente, estás cerrando el presupuesto a esa cantidad fija. Y antes de decirle ese número a tu cliente tené en cuenta sobre todo **cuanto te saldrá hacerlo a ti** para por lo menos no trabajar a pérdida, ya que en este tipo de contratación la mayoría del riego se lo lleva el que presta el servicio.

Si bien se dice que es un presupuesto cerrado, podemos tener algunas variaciones propias del acuerdo entre dos privados. De las cuales quiero destacar algunos tipos:

**Presupuesto cerrado rígido**: lo más conocido, se acuerda u ofrece una cantidad a pagar y esa cantidad no se cambia.

¿Y porque cambiaría? si reflexionas sobre tu actividad y qué factores hacen que cambien las condiciones de un proyecto o la prestación de un servicio notarás que pueden surgir imprevistos y en algunas disciplinas incluso se manejan muchas cuestiones con incertidumbre y cosas poco definidas. Estos son los grandes **riesgos** a tratar de administrar; la mejor manera de hacerlo es acortando y **limitando el alcance** general del trabajo. Es decir desde dónde hasta dónde llega el servicio que vas a prestar, o que incluirá el producto final; tratando de ser lo más precisos posibles en esta definición.

**Presupuesto cerrado con incentivos**: se acuerda u ofrece una cantidad a pagar y esa cantidad no se cambia. Pero además se pueden acordar ciertos incentivos extras para llegar a buen puerto.

-   Si se termina el trabajo antes de tal fecha, se entregará un monto extra por el valor del 10% del proyecto.
-   Si además del trabajo básico, se alcanza un objetivo secundario se cobrará X monto de dinero extra.

**Presupuesto cerrado con ajuste de precio:** (en inglés fixed-price with economic price adjustment) para proyectos de mediano o largo plazo, y sobre todo en economías inestables, se suele acordar también un ajuste del precio cada cierto tiempo.

Una forma de hacer esto de forma automática es atar el precio a una moneda estable, generalmente el dólar americano.

**Pago por Etapas**: sobre la base de un presupuesto fijo se acuerda el pago por etapas o fases a medida que estas se alcanzan o concretan.

Esta modalidad reduce el riego de ambas partes ya que pudieran terminar su relación al alcanzar una etapa determinada.

**TIPS**: A tener en cuenta, para presupuestos cerrados:

-   La clave es medir el alcance del proyecto.
-   Ojo con los costos extras quien los paga.
-   Ojo con el ajuste por inflación para proyectos que lleven mucho tiempo.
-   Siempre es mejor agregar en tus cálculos, algún tipo de holgura o margen, en la jerga se le dice “buffer”, que te permita soportar algún imprevisto.
-   Ojo con pasarte de horas/costos muy seguido.

**2.1.2 Costos + Porcentaje.**

(en inglés cost-plus fixed fee contract)

Es un tipo de contrato en donde se paga por todos los costos en los que incurrió el que presta el servicio más un porcentaje fijo de ganancia. También se puede acordar solo pagar por un porcentaje sobre los gastos extras en los que se incurran.

Si bien es un tipo de relación más sofisticada y poco común en mi caso, se da bastante en proyectos de construcción o remodelación donde por ejemplo un Arquitecto te dice “te cobro los materiales más la mano de obra”, aunque no se acuerde ese porcentaje de ganancia estará prorrateado en la mano de obra e incluso en los materiales.

**2.1.3 Mensualidad.**

En este tipo de contratos el pago se hace a **montos fijos por mes** calendario. Según cómo y lo que se acuerde puede incluir una cantidad infinita de prestación de servicio.

Por ejemplo, unos amigos que hacen mantenimiento preventivo de informática, realizan como mínimo 1 visita mensual a sus clientes, pero están todo el mes en guardia para atender y resolver cualquier imprevisto. Es rentable cuando se hace la visita regular y se puede tornar un dolor de cabeza si tienen que realizar visitas todos los días del mes.

Aquí también es recomendable realizar ajustes en el precio para mantener la ecuación financiera o ajustar a una moneda extranjera estable.

# **Por tiempo.**

Cobrar por **el tiempo incurrido** en la prestación del servicio es otra de las formas más comunes de acordar el pago.

Por ejemplo cuando visitas a un profesional de la salud este te cobra un monto por la consulta, con consultas que pueden tardar entre 15 a 45 minutos, similar para otras profesiones, es así dicen desde las más antiguas del mundo.

El pago por un **jornal** es otra forma de tiempo limitado a un máximo legal de 8 horas, dependiendo de la legislación local donde te encuentres.

A continuación quiero destacar una de las formas que más comúnmente define relaciones comerciales a mediano y largo plazo en los prestadores de servicios.

**2.2.1 Tiempo y Materiales** (en inglés mejor conocido como Time & Materials)

En este tipo de relación se acuerda **pagar por el tiempo incurrido al prestar el servicio y cubrir los costos de materiales/insumos en los que se incurra**. Simplemente se puede cobrar por la cantidad de horas incurridas en total para completar una tarea o completar la creación de un producto.

Al momento de hacer el acuerdo simplemente se puede fijar un precio de hora de servicio y definir una manera de cómo se va a registrar.

**Paquete de horas**: para mejorar la previsibilidad del negocio, una práctica muy común en la industria de servicios es comercializar las horas en paquetes. Por ejemplo 200 horas, donde el comprador solicita esa cantidad por un monto fijo de dinero y el prestador se compromete a realizar su trabajo por esa cantidad de horas.

Calcular cuanto te sale producir esa hora de trabajo será la clave y la diferencia entre un negocio financieramente exitoso de uno que de perdidas. Es tan amplio el tema que lo dejo para otro articulo. Mientras tanto te recomiendo tener en cuenta al menos ciertos ítems como: tus costos, tu remuneración como auto-empleado y tu margen de ganancia para calcular el valor comercial de tu hora.

A tener en cuenta:

-   Ojo con el ajuste de precio hora para contratos a largo plazo. Ponelo en dólares si estas en contexto inflacionario a “cotización oficial del banco nación al momento del pago” o también he visto “cotización oficial del banco nación al cierre del día anterior al pago”.
-   Ojo no pasarse de horas y del presupuesto real de tu cliente.
-   Acordá cómo vas a registrar las horas trabajadas. Yo recomiendo la herramienta Toggl.com, por su facilidad de uso y disponibilidad en diferentes plataformas.

# **Por valor**.

(en inglés Value Based price)

En esta modalidad se cobrará por **el valor que se genera para el negocio**, generalmente un porcentaje de la ganancia futura que tendrá el cliente como resultado de nuestro trabajo.

Ilustremos el concepto con una pequeña historia: En Agosto de 1999, el grupo económico _Citigroup_ que se había fusionado con el grupo económico _Travelers_ (cuyo logo era un paraguas rojo) encargó el diseño de una nueva identidad a una empresa; sucedió que la diseñadora Paula Scher, que trabajaba en ese estudio y tenía una carrera de 34 años, en la primer reunión al ver las identidades del _citigroup_ y _travelers_ ideó en una servilleta de papel y en **10 minutos** el logotipo que sería la fusión de ambos grupos y se dice que cobraron **1.5 millones** de dólares por ese trabajo. También se dice que cuando su cliente dijo que no iba a pagar por algo que se creó rápido la diseñadora le contestó que ella le estaba vendiendo 10 minutos de toda una vida de experiencia profesional. En su charla TED cuenta que si bien el logo lo hizo en 10 minutos tardó más de 1 año en convencer a toda la corporación que era el diseño correcto.

> “Fue un segundo hecho en 34 años” — Paula Sche

![](https://miro.medium.com/max/700/1*8lEL09O-XF37WDCCd5EhPA.png)

Fig. 3: Captura de la charla TED donde Paula Sche muestra el prototipo del logo del Citibank

Puedes ver su charla TED en [https://www.ted.com/talks/paula_scher_great_design_is_serious_not_solemn/transcript?language=es](https://www.ted.com/talks/paula_scher_great_design_is_serious_not_solemn/transcript?language=es)

**¿Cuanto vale la hora de Paula?** quinientos, mil, cinco mil dólares eso no importa, en este modelo lo que realmente importa es **¿Cuánto valor le agrega a la marca Citibank su diseño?** Cual podría ser el Retorno de la Inversión (ROI) si los consumidores identifican la marca con la empresa y los invita a ser sus clientes.

Cobrar por el valor que se agrega es la forma más sofisticada de cobrar por un trabajo, con un cambio total de chip, bajo la premisa de **“No cobro por lo que sé, cobro por el valor que agregó a tu negocio**”, se necesita conocer mejor el negocio del cliente y el resultado esperado de tu trabajo y como este pueden impactar positivamente a ese negocio.

Pongamos otro ejemplo, un ingeniero industrial optimiza un proceso que ahorrar a una cervecería $ 1 millón al año en mano de obra, puede pedir sus $50.000 de honorarios porque le llevo 1 mes de trabajo, pero también podría argumentar que su compensación sea el 10 % del ahorro, es decir $100.000. Y por el mismo tiempo duplicar su ingreso.

Un diseñador, cambiá el sitio web de un supermercado mayorista, lo que produce un aumento 40% en las ventas en tiempos de COVID al mejorar la comunicación del negocio. Puede cobrar sus $35.000 de honorarios por un trabajo de 15 días. Pero en este modelo podía haber negociado recibir un bono por el 2 % de las ventas que se den en la nueva plataforma. Como vendieron $ 15 millones, su comisión podría haber sido de $ 300.000

Esta modalidad es utilizada en mercados donde la “emoción” tiene un rol principal, como la moda; cuando hay exceso de demanda, en productos complementarios (la tinta de la impresora, los auriculares de tu teléfono móvil) e incluso en la industria automotriz de alta gama.

# Fin del recorrido.

Espero que luego de esta recorrida te sirva para conocer algunas de las alternativas que tiene disponible y puedas seguir investigando cuál es la mejor forma de cobrar para tu negocio. También que te invite a la reflexión sobre las decisiones que tomas al aplicar cualquiera de estas modalidades. Y si no realizar trabajo en forma independiente espero te sirva para generar empatía cuando contrates los servicios de alguien más.

Cuánto cobrar, cómo cobrar y cuánto me sale el hacer, lo dejamos para otro artículo.

# ¿Has cobrado de alguna otra forma? espero tus comentarios.

PD-1: pongo los términos como se los conoce en **inglés** para que te ayude a profundizar la búsqueda y porque tanto la bibliografía y los contenidos en línea más actuales sobre estos temas está en ese idioma.

PD-2: Sobre aspectos legales cuando trabajar de forma independiente, y de forma remota, te recomiendo esta charla **“ExportAR: Aspectos Legales del Trabajo Remoto — Silvio Messin”** de la conferencia Nerdearla, que habla del tema. Ver charla en [https://www.youtube.com/watch?v=IVkpHcB0sb8](https://www.youtube.com/watch?v=IVkpHcB0sb8)

[![VIDEO YOUTUBE](http://img.youtube.com/vi/IVkpHcB0sb8/0.jpg)](http://www.youtube.com/watch?v=IVkpHcB0sb8)

…