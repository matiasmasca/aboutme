---
layout: post
title: '¿Por qué volvería a usar reloj?'
date: 2020-01-18 22:44
categories: blog
locale: es
image: ''
headerImage: false
tag:
- life
draft: false
author: matiasmasca
description: 'Hacía años que no portaba ningún objeto en la muñeca. Aunque había recibido varios, como regalo, a lo largo del tiempo, ahora la obsesión por mirar la hora va mucho más allá. La nueva adquisición me permite estar más consciente sobre mi estilo de vida. Para quedarse tranquilo, este nuevo “reloj”  también da la hora, y no tiene nada que ver con una pulsera de prisión.'
---

> *- ¿Mamá por qué usas reloj, si solo sirve para una cosa: mirar la hora, mientras que con mi celu hago de todo?* \
> le escuché decir a una niña en la calle. \
> Miré mi muñeca y no tenía reloj, sino una pulsera negra.

Pasaron muchos años desde que había usado reloj de muñeca por última vez, cuando me dí cuenta que vivía apurado y casi obsesionado, mirando la hora cada 5 minutos o menos. Yo no tenía una buena respuesta para la pregunta de esa nena. Familiares, novia, amigos me han regalado algunos, que terminaron archivados en algún cajón, hasta que se les acabo la pila. Confieso que en alguna ocasión he usado el más elegante, pero como accesorio de vestir, aunque diera mal la hora.

Este año volví a usar algo permanente en mi muñeca que está cambiando mi vida. Se parece a un reloj de muñeca, pero sin agujas: fueron remplazadas por una pantalla negra; sin cuerda, ni pilas; a cambio tiene una batería recargable que no dura años sino semanas y tampoco se llama reloj de muñeca. Lo que sí se mantiene es que poco a poco me está volviendo ese habito, casi obsesión, de mirarlo a cada rato, pero esta vez no es solo la hora lo que miro.

## Tecnología vestible

Lo que uso ahora se lo conoce de varias formas como "fitness band", "smart band" (pulsera inteligente), o"smart watch" (reloj inteligente); aunque el nombre correcto (pero poco marketinero) es Monitor de Actividad (en inglés 'activity tracker').

Es un pequeño [dispositivo para medir ciertos parámetros corporales](https://es.wikipedia.org/wiki/Seguidor_de_actividad) y monitorizar otros que podemos vestir cómoda y cotidianamente, al contrario de sus pares médicos que para ser más precios requieren de otro tipo de tecnología. Y es parte de una línea de innovación o tendencia conocida como [Tecnología Vestible](https://es.wikipedia.org/wiki/Tecnolog%C3%ADa_vestible) (del inglés 'wearable technology')-, que poco a poco se va ganando su espacio entre las personas de a pie; tendencias que antes tardaban 10 o 15 años en llegar a esta zona del planeta y ahora tardan 2 o 3.

> Existen dispositivos vestibles o "werables" que nos permiten realizar mediciones sobre nuestro cuerpo, enviar esos datos por tu teléfono celular y visualizarlos de manera simple desde una aplicación. Y en algunos casos generar alarmas.


## Alertas accesibles**

Para mejorar algo, primero debemos aceptar que existe algún factor que no está del todo bien. Luego debemos de alguna manera medir qué tan lejos o cerca estamos de una situación ideal. ¿Cómo? Y sobre todo ¿cada cuánto medís tu salud?

En mi caso la respuesta era "prácticamente nunca y solo cuando me siento muy mal", pero este año descubrí que mi presión arterial suele subir a 19/10 sin que me dé cuenta, la razón: stress laboral y sendetarismo, lo mismo que mucha gente.

Entonces busqué algo que me ayudara a medir mi falta de actividad física e investigué un poco sobre esas "banditas", de las que ya había escuchado. Para mi sorpresa, tenía un costo accesible de $1.500 pesos, como todo hay más baratas y más caras. También vi que las venden en un local céntrico de Corrientes.

![<%= relative_url 'images/posts/band4-5-xiaomi-corrientes-1024x756.png' %>](<%= relative_url 'images/posts/band4-5-xiaomi-corrientes-1024x756.png' %>)
Smart band -- marca Xiaomi

Este dispositivo vestible, me permite: Medir cada 5 o 10 minutos mi ritmo cardiaco (lamentablemente, no mi presión arterial), cuantos pasos hago al día, cuantas calorías se queman, calcular cuánto y cómo duermo, y alertarme si paso mucho tiempo setando.

> Conectado con mi teléfono celular, con una aplicación del fabricante, puedo visualizar todos esos datos de forma sencilla y ver los cambios en el tiempo.

Esto me sirve en esencia para mejorar mi salud y mi estilo de vida, al estar consiente de esos valores. Si un día normal veo que mis pasos están muy por debajo de lo recomendado, por más cansancio que tenga, trato de salir a caminar por al menos 40 minutos. Si veo que dormí profundamente pocas horas, pienso en qué hice o qué comí, a qué hora y al otro día pruebo hacer algo distinto para descansar mejor.

Al tener un historial, puedo ver la evolución o involución a lo largo del tiempo: esta semana anduve flojo, tendré que salir a caminar el sábado también. A veces me funciona, otras no. Lo importante es estar de cierto modo consiente de nuestro estilo de vida y en cómo afecta nuestra salud de una manera simple, que no ocupe muchos "recursos cognitivos", como dice el Dr. Manes.

No falta mucho para que en vez de un mensaje de WhatsApp o Telegram, recibas una notificación que diga: "Pará un poquito con la sal, que está subiendo tu presión" o "Tómese el siguiente taxi, probabilidad de ataque cardiaco es 98,7%", enviados por su asistente inteligente de salud.

Me imagino un sistema de salud optimizado con esos datos, donde el impacto de las políticas públicas pueda realmente medirse y ajustarse en tiempo "casi" inmediato, incluso personalizarse justificadamente a cada ciudadano. No estamos lejos de eso, ni es tan costoso de llevar a la práctica. Claro que aún tenemos el factor más limitante: el "humano".

![<%= relative_url 'images/posts/moreyra-smart-watch-512x1024.jpeg' %>](<%= relative_url 'images/posts/moreyra-smart-watch-512x1024.jpeg' %>)

## ¿A dónde van los datos?

Más allá de mi positivismo de tecnólogo, vivimos en una sociedad donde la información es poder y donde los datos, tus datos, se vuelven dinero para algunos que están constantemente hurgando en ellos, entonces es válido preguntarse ¿Qué y a quién se envía esa información?

Cuando una aplicación tiene acceso a tus datos y una conexión a Internet tiene porque vos aceptaste unos términos y condiciones de uso (que ni leíste) que se lo permiten.

> Nadie sabe a ciencia cierta a dónde van, quién los recibe y qué se hacen con ellos. Aunque en la industria de la publicidad y el consumo, sí saben qué hacer con ellos.

Publicidades dirigidas a vos, hoy son posibles, gracias a esas aplicaciones que tienes en tu "teléfono celular inteligente" que informan qué haces, con quién te vinculas, dónde estas, dónde estuviste, de qué hablas, qué te gusta y qué te molesta. Y podrían muy fácilmente sugerirte insistentemente que consumos X o Y producto de "venta libre" para bajar esos niveles altos que están sabiendo que tenés. Tan así es la industria de los datos tuyos que hoy pueden manipular a una sociedad entera para que vaya en una u otra dirección según su propia conveniencia.

## Esperar el momento

La facilidad de acceso y los relativos bajos costos de estas nuevas tecnologías hacen que esperemos avances significativos en el cuidado de la salud personal. Quiero ser optimista al pensar que usaremos esta nueva información para impactar positivamente en nuestra calidad de vida y a larga hacer más duradero nuestro viaje.

Un sistema de salud, tanto público como privado, optimizado y eficiente hoy es posible, incluso en una de las regiones más pobres del país; donde se gasta más en una fiesta de 1 día que en una capacitación o en un equipamiento que dure varios años.

> Cuando llegue ese día, que va a llegar, también tendrás que preguntarte y exigir que te informen ¿Qué datos tuyos tienen, quién tiene o tuvo acceso a ellos y que uso hacen?

Mientras tanto, necesitamos de profesionales de la salud "amigados" con la tecnología, que realmente la entiendan y que sepan hacer uso de ella para optimizar los diagnósticos y tratamientos. Necesitamos de funcionarios comprometidos con la necesidad y calidad de esos datos para su impacto positivo en todo el sistema de salud, sobre todo en materia de prevención.

¿Cuántos de nuestros parientes y amigos podrían hoy estar entre nosotros si hubieran tenido una alerta temprana de que algo en su sistema corporal no estaba funcionando bien y que debían dirigirse a un centro de salud a la brevedad?. Seguro que alguien, igual de terco, la hubiera apagado y exigido un salero en la mesa.

En este momento acaba de vibrar el dispositivo en mi muñeca indicado "Alerta de inactividad", la mente puede estar escribiendo pero el cuerpo necesita estar en movimiento, es momento de hacer una "pausa activa".

Publicado originalmente el 18 de Enero de 2020 en el medio de comunicación Cronicas de Agua en [https://cronicasdeagua.com/por-que-volveria-a-usar-reloj/](https://cronicasdeagua.com/)
