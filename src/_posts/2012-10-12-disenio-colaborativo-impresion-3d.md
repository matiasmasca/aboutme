---
layout: post
title: 'Diseño colaborativo para durar de distribución global'
date: 2012-10-12 22:00:00 -0300
categories: blog
locale: es
image: ""
headerImage: false
tag:
- hardware libre
- impresion 3D
- Obsolescencia programada
- crowdfounding
- Hours Of Service
author: matiasmasca
description: Reflexión sobre la impresion 3D, la obsolecencia programada y la distribusción de productos fisicos
---

Los productos que utilizamos a diario, en su gran mayoría, se han diseñado para romperse o dejar de funcionar al cabo de un tiempo, esto es la llamada “**Obsolescencia Programada**”. Y sólo, en el mejor de los casos, los dejamos de utilizar por considerarlos inútiles aunque técnicamente funcionen sin problemas, esto último se lo conoce como “**Obsolescencia Percibida**”.

El concepto que se ha llamado “Obsolescencia Programada” fue propuesto por Bernard London en 1932 como una medida para paliar la gran depresión de los Estados Unidos, quien además pretendía que este concepto fuera obligatorio por ley para todo aquel producto manufacturado, es decir por norma todo producto tendría que tener una fecha preestablecida de caducidad, pero esta iniciativa no prosperó. Luego en 1954, el diseñador industrial estadounidense Brooks Stevens, populariza el término entre sus colegas y en la industria, según se cuenta en el documental emitido en TV Española “Comprar, Tirar, Comprar” [1] dirigido por Cosima Dannoritzer, sobre esta temática también se hace referencia el libro/documental “La Historia de las Cosas” de Annie Leonard [2]. En definitiva el concepto de “Obsolescencia Programada” no es un mito urbano sino un invento de la modernidad con el que sin saberlo vivimos día a día aceptándolo como normal y, al parecer, seguiremos viviendo así por mucho tiempo; pero *¿cuánto tiempo podrá soportar el planeta y la sociedad este tipo de sistemas lineales que requieren infinitos recursos?* No tengo la respuesta, pero sí puedo decir que se empiezan a notar síntomas de desgaste (como la contaminación ambiental, el calentamiento global, etc.) que nos hace pensar que el camino actual no es el correcto.

**¿Dónde nace la obsolescencia?** En el documental “Comprar, Tirar, Comprar” se explica que los ingenieros industriales se ven obligados por las empresas a diseñar productos que duren el tiempo exacto para que dejen de servir sin perder la confianza del cliente, pero que si o si se dañen o sean frágiles en ciertos puntos (como las sillas de plástico), cuando perfectamente tenemos el conocimiento técnico para hacerlos durar un tiempo mucho mayor. Este también da a entender que desde la misma academia, se los insta a seguir con las demandas de la industria para obtener o conservar un empleo. Allí también se muestran casos reales como el cartel Phoebus que redujo la vida útil de las bombillas eléctricas, y el caso de DuPont que luego de haber creado una fibra muy resistente ordenó a sus ingenieros empezar de nuevo y diseñar fibras muchos más frágiles. Es difícil de creer como personas que esperamos lleven su profesión con ética y la búsqueda del desarrollo de la sociedad, terminan realizando estas prácticas de dudosa ética y que en definitiva perjudican a la sociedad y faltando a su juramento “**Prometo utilizar todos mis conocimientos, experiencia y compromiso para lograr una productividad al servicio del desarrollo de los seres humanos y en armonía con la preservación de la naturaleza**”. Este es un sistema distribuido totalmente en nuestra cultura y con ciertos preceptos que lo arraigan en toda la sociedad moderna, por ejemplo “si las cosas no se rompen la rueda económica no gira”, “si las cosas no se dañan y reemplazan los obreros no tendrían trabajo”. Se entiende que es una temática compleja dada la forma en que quedó estructurada la sociedad luego de la revolución industrial. Sin embargo ¿Dónde quedan los valores? ¿Dónde queda la ética profesional del trabajo? ¿Por qué aplican todo el conocimiento para hacer que las cosas funcionen mal cuando podrían invertir en mejora contínua e innovación?

## El “Hardware Libre”

Como un tsunami en el mar, casi imperceptible, viene surgiendo otra forma de pensar, que en un futuro cuando llegue a costa puede cambiar la manera en la que las empresas manufactureras hacen sus negocios; esta corriente de pensamiento, inspirada en el mundo del software, se llama “Hardware Libre”. La misma consiste en poner en dominio público todas las especificaciones de diseño de un producto para que cualquier persona pueda estudiar, modificar y compartir esa modificación con el resto del mundo. De esta manera, a mi entender, se podrían lograr productos diseñados funcionalmente para durar mejorando sistemáticamente sus puntos de fallo, se podría pensar colaborativamente en utilizar insumos alternativos para un mismo producto, progresivamente se podría evitar la lucha de patentes que hoy frena la innovación en los Estados Unidos, se podría trabajar sobre productos que la industria en general no desea trabajar (nichos desatendidos por su poca rentabilidad) o aquellos que fabricándolos ponen en riesgo el statu quo de otros productos bien instaurados en la economía global. Incluso trabajar en alternativas para aquellos productos que tienen asociados modelos de negocios viciados o dañinos para la sociedad.

Esta corriente de pensamiento viene surgiendo en los ambientes académicos, donde en la mayoría de los casos se encuentran con jóvenes idealistas llenos de ideas y con el tiempo libre como para contribuir a la sociedad desinteresadamente con el único fin de poner en práctica sus conocimientos al tiempo que realizan una función de devolución social; estos en general se están inspirando en sus colegas informáticos cuyo proyectos de software libre tienen hoy un impacto global, por ejemplo Apache es el servidor de páginas Web más utilizado en 2012 según un estudio de Netcraft [3] sobre una base de 620 millones de páginas. Imagínense las mejoras que podrían realizar estos jóvenes llenos de conocimiento e ideas si imitaran el éxito del software libre y se difundiera entre ellos esta cultura de democratizar el conocimiento. Un ejemplo de esta corriente es el repositorio de Open Hardware Repository del CERN [4] que busca alentar el diseño electrónico colaborativo, pero para ello ha desarrollado una licencia llamada “Open Hardware License”, esta otorga a los usuarios la libertad de estudiar, modificar, redistribuir y producir documentación de diseño, pero estipula que las obras derivadas de la documentación deben liberarse bajo los mismos términos. Entonces, a mi entender, las empresas manufactureras tendrían que aprender e imitar las lecciones del Software Libre que tiene sus propios mecanismos para generar empresas de clase mundial e innovación constante y sin perjuicio de esto compartir en versiones comunitarias todo ese conocimiento. El foco en estos casos para las empresas tendría que ser las licencias, ya que así como hay “Software Libre” y “Software de Código Abierto” y estos difieren básicamente en sus licencias, en la libertad de lo que el usuario puede hacer con ellos, podría haber Hardware Libre, Hardware de Fuentes Abiertas (OSHW en inglés) o el Hardware Abierto; sobre este último hace algunos años recuerdo haber leído sobre una empresa de tecnología que había liberado las especificaciones de las carcasas de sus notebook (computadoras portátiles) para que cualquiera las pueda personalizar fue una noticia destacada en su momento, más recientemente puedo citar el caso de Sony quien el año pasado liberó las especificaciones para la montura de lentes externos para algunas de sus cámaras de foto, así los fabricantes de lentes podrían adaptarlos a sus productos [5] pero estos no son ejemplos de Hardware Libre, sino más bien de Open Desing (Diseño Abierto) que son pequeños pasos previos para un sector industrial tradicionalmente hermético. Siguiendo con el hardware libre, otro proyecto importante de mencionar, nacido en el seno de la academia, es **Arduino** “... una plataforma de electrónica abierta para la creación de prototipos basada en software y hardware flexibles y fáciles de usar”[6] que si bien tiene fines académicos de prototipado esta teniendo un desarrollo muy interesante a nivel mundial por su cantidad de adeptos, ya que las placas pueden ser hechas a mano o comprarse montadas en una fábrica a unos valores muy accesibles.

Un problema, que enfrenta hoy el Hardware Libre, es la disponibilidad de exactamente los mismos componentes en todo el globo, de modo que cualquier usuario los pueda construir y terminar su proyecto, lo que en muchos casos eleva el costo de estas soluciones; pero estará en astucia de las comunidades encontrar componente alternativos, hasta modificaciones, para lograr que el diseño funcione con componente que estén al alcance.

En el mundo del software libre es muy común que empresas que no pueden continuar de forma cerrada con el desarrollo de un producto, ya sea por cuestiones presupuestarias o de poco éxito comercial, lo liberen a la comunidad para que esta continúe con el desarrollo, como fue el caso de WebOS de HP por mencionar un caso. Así empresas con productos abandonados o estancados podrían liberarlos totalmente o en parte para que la comunidad innove sobre ellos. Un ejemplo es la iniciativa OpenSparc de la empresa Sun Microsystem (ahora propiedad de Oracle) que liberó las especificaciones de procesadores de 64bit [7].

Tener disponible este conocimiento no hace que todos los usuarios reemplacen a los profesionales, como la industria de los muebles no fue destruida por los muebles armados en casa, en el hardware libre incluso muchos profesionales compran las placas montadas en fábrica de Arduino por su calidad y confianza, cuando fácilmente las podrían montar en sus casas. Y aunque este movimiento tuviera una aceptación total aún habría muchos componentes que comprar a las fábricas y muchos eslabones donde agregar valor.

Sobre los posibles problemas asociados al hardware cerrado recientemente era noticia como una comisión parlamentaria de los Estados Unidos acusaba de “posible” espionaje y de riesgo para la seguridad de su país a una empresa de origen chino llamada Huawei (Huawei Technologies Co. Ltd) y que hoy posee una participación mayoritaria en el mercado de las telecomunicaciones [8]. Por ejemplo en Argentina los MODEM ADSL entregados masivamente por las gigantes como Telecom o Telefónica eran de esta empresa. Pero si las especificaciones hubieran estado abiertas, no digo libres sino en principio sólo abiertas, este problema no hubiese existido. Este problema de seguridad por desconocimiento del funcionamiento exacto de muchos aparatos electrónicos, sobre todo los de comunicación, es parte del problema de los que Estados que hoy se conoce como “Soberanía Tecnológica”. Podemos entender como tal al el derecho y el deber de una nación de dominar sus medios tecnológicos, es decir el Estado es el que tiene que tener control de la tecnología que utiliza y no al revés; este concepto empieza a cobrar relevancia y agenda en los estados de todo el mundo. Pienso que este tema será uno de los potenciadores de la adopción del Software Libre en el estado Argentino, pero también lo están tratando otros estados, muestra de ello es el tercer coloquio sobre Independencia de Europa y Soberanía Tecnológica llevado a cabo los pasados 24 y 25 de noviembre en Augsburgo. Y luego en el futuro, esto podría seguir la tendencia hacia el Hardware Libre.

## La Impresión 3D

Por otro lado, los invito a cerrar los ojos por unos segundos e imaginar un mundo en el que cuando necesite ciertos elementos, en vez de salir a comprarlos, usted diga “Computadora, crear un perchero para tazas individual” y unos minutos después la pieza de algún tipo de polímero ecológico esté lista para ser instalada en la cocina. Imaginemos un mundo en el que es el cumpleaños de algún ser querido y usted diga “Computadora, quiero un bombón de chocolate con forma de guitarra eléctrica” minutos después varias y perfectas guitarras en miniatura de chocolate están listas para comer. En otro caso su proveedor de diseños le pide acceso para imprimir directamente en su casa el nuevo diseño de bombones para el día de los enamorados, este es un posible modelo de negocios. Ese mundo está a la vuelta de la esquina con las llamadas “Impresoras 3D”, aparatos electrónicos capaces de realizar impresiones, de diseños en 3D, creando piezas volumétricas a partir de un diseño hecho por computadora. Estos diseños son para el usuario común un mero archivo que enviarán a la impresora de igual forma que lo hacen hoy con cualquier documento, al mismo tiempo podrán ser creados por él o por un profesional. Como insumo utilizarán algún tipo de cartucho con diferentes materiales naturales o sintéticos; aquí las empresas podrían crear sus propios insumos exclusivos para estos aparatos. Ya han surgido proyectos como fab@home [9] o el RepRap [10] que permitirán a cualquier persona la fabricación de objetos para su vida cotidiana. El proyecto RepRap puntualmente pretende que la máquina ayude a construirse o replicarse a sí misma. Son muchísimos los usos que se les puede dar a estos dispositivos, me imagino la capacidad de crear piezas ortopédicas como un exoesqueleto, piezas de diferentes tamaños y formas, juegos didácticos de dominio público, piezas de rompecabezas, incluso rompecabezas en 3D. Las marcas podrían permitir a sus fans imprimir directamente el merchandising.

![3d printer](http://upload.wikimedia.org/wikipedia/commons/thumb/2/22/WCN_2011_3D_printer.jpg/640px-WCN_2011_3D_printer.jpg)

Obviamente aún faltan muchos pasos para llegar al punto de poder fabricar en casa absolutamente todos los elementos que necesitaría nuestro proyecto con “hardware abierto”. Me imagino que al principio habrá una impresora 3D por ciudad, luego una por barrio, una por manzana, una por cuadra. Al principio serán ruidosas y lentas, como aquellas viejas impresoras de matriz de punto, pero luego en el futuro serán rápidas y silenciosas, como las modernas impresoras láser o las de inyección de tinta. Combinando diferentes materiales y formas de unirlos, hasta poder lograr productos electrónicos totalmente funcionales. Y si desde un principio son construidas como Hardware Libre no tendrán ese chip que las haga dejar de funcionar al cabo de un tiempo.

## La distribucion global

Actualmente los costos de transporte de un producto manufacturado representan entre un 20% y 50% del costo del mismo, esto depende de cada país, imagínense la reducción de costos que la impresión 3D con diseños libre podría traer. Y si tuviéramos que enviar un repuesto al espacio exterior, hoy poner en órbita de la tierra un kilo de carga útil le cuesta a la NASA aproximadamente $ 22000 dolares [12], la joven empresa Made in Spaces [13] esta trabajando en hacer funcionar una impresora 3D en gravedad cero. Estas impresoras hoy dan la posibilidad de la distribución de global de algunas piezas a un costo bastante bajo, incluso pueden saltar las barreras de entrada a distintos países ya que al ser una nueva tecnología, aún no hay regulaciones al respecto alrededor de ellas.

Cuando se manifieste la resistencia al cambio, cuando la industria quiera frenar los avances de estas tendencias o estos productos novedosos, existirá la posibilidad del crowdfounding (financiación colectiva) para llevarlos adelante; como es el actual caso de éxito de la consola “Ouya” que ya cuenta con 8,5 millones de dólares estadounidenses para su financiación en el sitio de cofinanciación Kickstarter [13], si bien el hardware no es libre, su plataforma de software sí lo es ya que implementará el popular sistema operativo libre Android.

**Creo que cuando un grupo de personas colectivamente se imaginan algo es cuestión de tiempo y esfuerzo para que esa idea se materialice y lo que ayer era sólo ciencia ficción, hoy con algunas variaciones, es nuestra realidad.**


Referencias:

[1]Documental “Comprar, Tirar, Comprar”. Dirigido por Cosima Dannoritzer. Emitido el día 20 de Abril de 2012. Por Corporación de Radio y Televisión Española 2012.  [http://www.rtve.es/television/documentales/comprar-tirar-comprar/](https://www.google.com/url?q=http://www.rtve.es/television/documentales/comprar-tirar-comprar/&sa=D&source=editors&ust=1674481919855819&usg=AOvVaw0TwFdGwzs80SNn-GenEGIR)

- Más información en Español [http://es.wikipedia.org/wiki/Comprar,_tirar,_comprar](https://www.google.com/url?q=http://es.wikipedia.org/wiki/Comprar,_tirar,_comprar&sa=D&source=editors&ust=1674481919856193&usg=AOvVaw1gDlc_vrd87iXo9Ckf525e)

[2] Documental “La Historia de las cosas” de Annie .  [http://www.storyofstuff.org/movies-all/story-of-stuff/](https://www.google.com/url?q=http://www.storyofstuff.org/movies-all/story-of-stuff/&sa=D&source=editors&ust=1674481919856886&usg=AOvVaw2CNhv76vWlEYYkRB7hGij9) - Más información en Español  [http://es.wikipedia.org/wiki/La_historia_de_las_cosas](https://www.google.com/url?q=http://es.wikipedia.org/wiki/La_historia_de_las_cosas&sa=D&source=editors&ust=1674481919857506&usg=AOvVaw00xGjwciXZbefKJRrbO4Dr)

[3] October 2012 Web Server Survey

[http://news.netcraft.com/archives/2012/10/02/october-2012-web-server-survey.html#more-6650](https://www.google.com/url?q=http://news.netcraft.com/archives/2012/10/02/october-2012-web-server-survey.html%23more-6650&sa=D&source=editors&ust=1674481919857984&usg=AOvVaw1iKJ6bymNkeOWHF09Fe7wl)

[4] Open Hardware Repository http://www.ohwr.org/

[5] Nota de prensa de Sony http://www.sony.co.jp/SonyInfo/News/Press/201102/11-018/ (en japonés)

[6] Proyecto Arduino  [http://www.arduino.cc/es/](https://www.google.com/url?q=http://www.arduino.cc/es/&sa=D&source=editors&ust=1674481919858760&usg=AOvVaw0FZfwUhAwBsAlMH85I_tT6)

[7] Proyecto Open Sparc http://www.opensparc.net/

[8] Noticia sobre Huawei

[http://www.bbc.co.uk/mundo/noticias/2012/10/121008_tecnologia_espionaje_eeuu_china_tech_companies_med.shtml](https://www.google.com/url?q=http://www.bbc.co.uk/mundo/noticias/2012/10/121008_tecnologia_espionaje_eeuu_china_tech_companies_med.shtml&sa=D&source=editors&ust=1674481919859247&usg=AOvVaw1k0X0qSIGR0b1v9UZSqKjW)

[9] Proyecto Fab@home  [http://www.fabathome.org/](https://www.google.com/url?q=http://www.fabathome.org/&sa=D&source=editors&ust=1674481919859844&usg=AOvVaw1X-ZroeTl0vCzBYQEADJ8C)

[10] Proyecto RepRap  [http://reprap.org](https://www.google.com/url?q=http://reprap.org/&sa=D&source=editors&ust=1674481919860359&usg=AOvVaw3hlU4jGFUxTl9ue14cWlRO)

[11] Empresa Made in Space, Inc. [http://madeinspace.us/](https://www.google.com/url?q=http://madeinspace.us/&sa=D&source=editors&ust=1674481919860725&usg=AOvVaw20M1cYgslcvkFnN18UVAVU)

[12] Artículo de la Nasa que cita el precio estimado de mandar 1 libra al espacio.

[http://www.nasa.gov/centers/marshall/news/background/facts/astp.html_prt.htm](https://www.google.com/url?q=http://www.nasa.gov/centers/marshall/news/background/facts/astp.html_prt.htm&sa=D&source=editors&ust=1674481919861189&usg=AOvVaw1jhvbghZy05WCnfWZVd2Xh)

[13] Proyecto Ouya  [http://www.kickstarter.com/projects/ouya/ouya-a-new-kind-of-video-game-console](https://www.google.com/url?q=http://www.kickstarter.com/projects/ouya/ouya-a-new-kind-of-video-game-console&sa=D&source=editors&ust=1674481919861923&usg=AOvVaw3_1jrMbAl6xs3V-GuqRhty)

* Últimas visitas a todas las fuentes el 30 de Octubre de 2012

Imagen 1: Impresora 3D por Ciell - Fuente:

[http://upload.wikimedia.org/wikipedia/commons/thumb/2/22/WCN_2011_3D_printer.jpg/640px-WCN_2011_3D_printer.jpg](https://www.google.com/url?q=http://upload.wikimedia.org/wikipedia/commons/thumb/2/22/WCN_2011_3D_printer.jpg/640px-WCN_2011_3D_printer.jpg&sa=D&source=editors&ust=1674481919862571&usg=AOvVaw1zv9-BfSX5Stdx_2hiJmsy)


## Historia

> *Este es el articulo que escribi y envie en el año 2012 para participar de un concurso de Estudiantes de Sistemas de la fundación
> Deloitte. Me dieron el 3re lugar. La impresion 3D en ese momento
> parerecia ciencia ficción para mi y era romota la posibilidad de poder
> acceder a una. Hoy hay servicios de impresion en muchicsimas ciudades*

![deloite winners group](<%= relative_url 'images/posts/premio_deloitte_01.jpg' %>#post-img)

![deloitte award certificate](<%= relative_url 'images/posts/premio_deloitte_02.jpg' %>#post-img)
