---
layout: post
title: 'Curso: Introducción a la programación con Python y juegos con Pygame'
date: 2020-04-07 22:00:00 -0300
categories: blog
locale: es
image: "/assets/images/markdown.jpg"
headerImage: false
tag:
- Python
- Aprender a programar
- En Español
author: matiasmasca
description: Apuntes y videos del curso de Introducción a la Programación con python y pygame
---


### Prologo
Acepté el desafío de enseñar a un grupo de jóvenes a programar desde 0 utilizando el lenguaje de programación Python, en su versión 3. Y luego aprovechando el encierro obligatorio, por la pandemia covid19, y que tuve que dar esas clases de forma virtual entonces me di a la tarea armar el material para compartirlo y así cualquier persona que tenga ganas de explorar la programación con Python pueda hacerlo.

Los videos están publicados en Youtube y el código en Github.

Es una versión Alpha, todavía me falta crear, grabar y subir las guías de trabajos prácticos, también las primeras clases donde explico que es un algoritmo. Es decir además de estos videos tuvimos sesiones en vivo de consultas o para prácticar, o ver las demos de los alumnos.

Bienvenidas las criticas y sugerencias, sobre todo del que tenga experiencia docente.

**Repositorio en Gibhub: ** ["https://github.com/matiasmasca/curso_python/"](https://github.com/matiasmasca/curso_python/)

## Curso: Introducción a la programación con Python y juegos con Pygame

Hasta el momento me base en un tutorial de Mike Dane, haciendo algunas adaptaciones y también he tomado notas del libro Invent with Python de Al Sweigart

Si estas aprendiendo Python los videos te pueden servir para ver un enfoque práctico sobre los conceptos generales de programación y algunos ejemplos de como aplicarlos con python.

### Clases

### Clase 1–3

En las primeras clases del curso presencial vimos algunos conceptos de la programación en general mediante la utilización de **Diagramas de Flujo** (o Flowchart) y **pseudocódigo**​.

< falta grabar esa clase >

También empezamos a utilizar la plataforma Mumuki, para reforzar los conceptos.

Para empezar, puedes crear una cuenta y hacer el curso de **Fundamentos de la programación** en la plataforma [https://mumuki.io/central](https://mumuki.io/central)

Si ya tiene algún conocimiento general sobre programación, puedes continuar.

### Clase 4 — Python 1

En esta clase empezamos a ver el lenguaje de programación Python.

Para poder empezar a probar sin instalar nada utilizamos la plataforma [repl.it](https://repl.it/)

Podes bajar e instalar un interprete de Python para tu sistema operativo desde [https://www.python.org/](https://www.python.org/)

Código de la clase: [v3] [acceder al código utilizado](https://github.com/matiasmasca/curso_python/tree/Clase01)

Ver clase en **YouTube** [https://www.youtube.com/watch?v=4M6PfqcCJcQ&feature=emb_logo](https://www.youtube.com/watch?v=4M6PfqcCJcQ&feature=emb_logo) [v2]

[![VIDEO YOUTUBE](http://img.youtube.com/vi/4M6PfqcCJcQ/0.jpg)](http://www.youtube.com/watch?v=4M6PfqcCJcQ){:target="_blank" rel="noopener"}

### Clase 5 — Python 2

En esta clase vemos los bucles de control del flujo While y For en Python.

También vemos las asignaciones multiples.

Los diccionarios

Manejo de excepciones o errores con Try

Lectura y escritura de archivos planos.

Código de la clase: [v2] [acceder al código utilizado](https://github.com/matiasmasca/curso_python/tree/Clase02)

Ver clase en **YouTube** PARTE 1 de 2: [https://www.youtube.com/watch?v=vC6iQ1I-oXs&feature=youtu.be](https://www.youtube.com/watch?v=vC6iQ1I-oXs&feature=youtu.be) [v2]

-   Bucle While
-   Bucle For

[![VIDEO YOUTUBE](http://img.youtube.com/vi/vC6iQ1I-oXs/0.jpg)](http://www.youtube.com/watch?v=vC6iQ1I-oXs){:target="_blank" rel="noopener"}

Ver clase en **YouTube** PARTE 2 de 2: [https://www.youtube.com/watch?v=vC6iQ1I-oXs&feature=youtu.be](https://www.youtube.com/watch?v=7VmfxEjyoHU&feature=youtu.be) [v2]

**Temas tratados en el video PARTE 2**

Listas de 2 dimensiones, grilla o matriz con lista de listas.
Asignación múltiple e interpolación de cadenas
Diccionario
Manejo de errores con Try y excepciones.
Lectura y escritura de archivos de texto plano.

[![VIDEO YOUTUBE](http://img.youtube.com/vi/7VmfxEjyoHU/0.jpg)](http://www.youtube.com/watch?v=7VmfxEjyoHU){:target="_blank" rel="noopener"}

### Clase 6 — Introducción a PyGame

¿ Qué vemos en la clase (vídeo) ?

-   Introducción a PyGame. Una librería que nos permite hacer programas multimedia y multiplataforma. Y en particular videojuegos.
-   Como ejecutar por primera vez pygame.
-   Objeto Ventana.
-   Como dibujar: polígonos, lineas, círculos, elipses y rectángulos

¿**Qué** es PyGame?

Pygame es un framework libre para Python que provee una serie de modulos diseñados para escribir programas multimadia, entre ellos videojuegos en 2D y 3D. Puede manejar tiempo, imagenes en diferentes formatos, video, sonidos, fuentes, cursores, mouse, teclado, joysticks y más; de una manera muy sencilla. La versión, al momento de grabar fue la 1.9.6, esta en desarrollo la 2.

Esta basada en una liberia llamada SDL (Simple DirectMedia Layer Library), escrita en C lo que le permite ser multiplataforma de manera simple y le agrega sus propias funciones.

Documentación: [https://github.com/pygame/pygame](https://github.com/pygame/pygame)

**Como instalar…**

-   Windows: busca un instalar aqui. [https://bitbucket.org/pygame/pygame/downloads/](https://bitbucket.org/pygame/pygame/downloads/)
-   Linux Ubuntu: ejecutar en la terminal: $ sudo apt-get install python-pygame o con pip: $ pip install pygame
-   Probar juego demo ejecutar en la terminal de comandos: $ python3 -m pygame.examples.aliens

Ver clase en **YouTube**: [https://www.youtube.com/watch?v=3zyn-4drvoI&feature=youtu.be](https://www.youtube.com/watch?v=3zyn-4drvoI&feature=youtu.be) [v2]

[![VIDEO YOUTUBE](http://img.youtube.com/vi/3zyn-4drvoI/0.jpg)](http://www.youtube.com/watch?v=3zyn-4drvoI){:target="_blank" rel="noopener"}

**Colores**

El sitio que se ve en el vídeo para elegir un color es Colores: [https://htmlcolorcodes.com/](https://htmlcolorcodes.com/)

La musica de fondo es [AudioLibrary Come Home — Declan DP](https://www.youtube.com/watch?v=MZ3HGnH50Zw&list=PLzCxunOM5WFIxNligdpQGH0seCt0DTH2q)

[https://github.com/matiasmasca/curso_python/tree/Pygame_clase1](https://github.com/matiasmasca/curso_python/tree/Pygame_clase1) Código de la clase: [acceder al código utilizado](https://github.com/matiasmasca/curso_python/tree/Pygame_clase1)

### Clase 7— Movimiento y colisiones en PyGame y uso elemental de Imágenes.

Demostración de código, continuación de la introducción a la librería PyGame. Se muestrá como mover un cuadrado sobre la pantalla, como detectar colisiones con otros objetos. También como cargar una imagen de un personaje e interactuar con ese cuadrado. Además de como mover el personaje con teclado. Luego como ejercicios los alumnos del curso tuvieron que practicar hacer algo similar. El que no tenia instalado lo necesario pudo hacerlo desde la herramienta online repl.it eligiendo en la lista de lenguajes “pygame”

Ver clase en **YouTube**: [https://www.youtube.com/watch?v=pVYjMFTMBr4&feature=youtu.be](https://www.youtube.com/watch?v=pVYjMFTMBr4&feature=youtu.be) [v2]

[![VIDEO YOUTUBE](http://img.youtube.com/vi/pVYjMFTMBr4/0.jpg)](http://www.youtube.com/watch?v=pVYjMFTMBr4)

Código: [https://github.com/matiasmasca/curso_python/tree/Pygame_clase2](https://github.com/matiasmasca/curso_python/tree/Pygame_clase2)

### Clase 8:

Demostración de código en vivo. Se aplico los visto en las clases pasadas para hacer un juego sencillo Brick (clon de Arkanoid). Se mostró como mover la imagen de la pelota y la paleta sobre la pantalla, como detectar colisiones en una lista para eliminar los bloques. Como mover el personaje con el mouse. Los alumnos fueron planteando situaciones alternativas al código generado y se modifico en vivo.

Como tarea los alumnos tienen que modificar el código para agregarle funcionalidades.

Código de la clase: [https://github.com/matiasmasca/curso_python/tree/Pygame_clase3](https://github.com/matiasmasca/curso_python/tree/Pygame_clase3)

## Clase 9:
Repaso de conceptos: que es un objeto Rect y un objeto Surface. Sistema de coordenadas.

Juego donde el personaje se va alimentando comiendo bacterias y va cambiando de tamaño usando la función Scale.


Ver clase en **YouTube**: [https://www.youtube.com/watch?v=1R_oK9cbC_g&feature=youtu.be](https://www.youtube.com/watch?v=1R_oK9cbC_g&feature=youtu.be) [v2]

[![VIDEO YOUTUBE](http://img.youtube.com/vi/1R_oK9cbC_g/0.jpg)](http://www.youtube.com/watch?v=1R_oK9cbC_g)

Código de la clase: [acceder al código utilizado](https://github.com/matiasmasca/curso_python/tree/Pygame_clase4)

## Clase 10:
Explicación del juego. Vimos la función move_ip (move in place), como mostrar textos en la pantalla, como por ejemplo el puntaje del jugador y una pantalla al inicio del juego.


Ver clase en **YouTube**: [https://www.youtube.com/watch?v=lWlleWddVBM&feature=youtu.be](https://www.youtube.com/watch?v=lWlleWddVBM&feature=youtu.be) [v2]

[![VIDEO YOUTUBE](http://img.youtube.com/vi/lWlleWddVBM/0.jpg)](http://www.youtube.com/watch?v=lWlleWddVBM)

Código de la clase: [acceder al código utilizado](https://github.com/matiasmasca/curso_python/tree/Pygame_clase5)

### Última versión:

Es recomendable es mirar el repositorio en GitHub para ver la última versión del curso, podes usar la opción para que te notifique de los cambios.

[https://github.com/matiasmasca/curso_python](https://github.com/matiasmasca/curso_python/tree/Pygame_clase1)


### Epílogo
Lo mi fue una experiencia, que duro solo ese semestre. Mi idea original era ir mejorando el material, los ejercicios y los videos para liberarlo directamente en Internet. Y para ir mejorando también como docente. Tengo la idea de hacer algo similar pero con el lenguaje Ruby, ojala algun día junte la energia para hacerlo.

¿Te imaginas si todos los docentes hicieran lo mismo? yo creo que la educación daría saltos de calidad

Agradecido con Sergio Lapertosa [@slapertosa](https://twitter.com/slapertosa) que confio en mi para esta aventura y al grupo de alumnos que me supieron aguantar y con los que aprendimos juntos.