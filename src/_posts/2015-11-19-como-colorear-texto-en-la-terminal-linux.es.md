---
layout: post
title: 'Ruby ¿Cómo colorear la consola? o el IRB'
date: 2015-11-19 22:00:00 -0300
categories: blog
locale: es
image: "images/posts/terminal-colors.png"
headerImage: false
tag:
- Ruby
- Linux terminal
- En Español
author: matiasmasca
description: Es posible cambiar el color de la consola bash en Linux y por lo tanto se lo puede hacer sencillamente desde Ruby
---

![linux terminal with colors](<%= relative_url 'images/posts/terminal-colors.png' %>)

Es posible cambiar el color de la consola bash en Linux y por lo tanto se lo puede hacer sencillamente desde Ruby. Esto nos podría servir para varias cosas, se me ocurre algún tipo de debugueo manual o para implementar nuestro propio Red-Green-Refactor.

## Cambiar el color de la fuente del prompt.

Aplicándole a una cadena antes de mostrar en pantalla un prefijo

    (“\e[x;ym”) y un sufijo (“\e[m”);

Por ejemplo:

    puts “\e[1;31mSoy un texto en rojo claro!\e[m”

Donde

    \e[ — Indica el comienzo del color del prompt.
    x;ym — Indica el codigo de color.
    \e[m — Indica el final del color del prompt

**Tabla de códigos de colores**:

    Negro (Black) 0;30
    Azul (Blue) 0;34
    Verde (Green) 0;32
    Celeste (Cyan) 0;36
    Rojo (Red) 0;31
    Fucsia (Purple) 0;35
    Marron (Brown) 0;33

Remplazar el 0 por un 1 para obtener colores Oscuros

    puts “\e[x;ymTu texto aqui\e[0m”
    puts “\e[1;31mSoy un texto en rojo claro!\e[0m”
    puts “\e[1;31mSoy un texto en oscuro claro!\e[0m”

## Resaltar el Texto, cambiando el fondo.

Al definición del color que ya teniamos se le agrega: e[47m\

    puts “\e[0;34m\e[47m\Texto Azul con fondo Blanco\e[m”
    puts “\e[0;47m\e[46m\Texto blanco con fondo Celeste\e[m”

**Tabla de Colores**:

    \e[40m #Negro
    \e[41m #Rojo
    \e[42m #Verde
    \e[43m #Amarillo
    \e[44m #Azul
    \e[45m #Fucsia
    \e[46m #Celeste
    \e[47m #Blanco

Adaptado de lo publicado en [http://www.thegeekstuff.com/2008/09/bash-shell-ps1-10-examples-to-make-your-linux-prompt-like-angelina-jolie/](http://www.thegeekstuff.com/2008/09/bash-shell-ps1-10-examples-to-make-your-linux-prompt-like-angelina-jolie/)