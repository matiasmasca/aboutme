---
position_title: "Full Stack Web Developer"
layout: job
date: 2017-02-01 22:00:00 -0300
start_date: 'Feb 2017'
end_date: 'Ago 2020'
place: Corrientes city, ARG
draft: false
view_more: true
category: jobs
company_name: Terciar Consultores
office_type: Remote
company_website: https://www.linkedin.com/company/terciar/
job_description: I collaborated to create some Ruby On Rails apps. For the government sector (Peteiha Trabajo) and for the education sector (Mi Cuaderno Digital). For the private sector in the category of funeral services (Aquaronte), for the category of fumigation services (Zapem), for the category of professional studies (App Novedades). I also participated in the development of projects and proposals for different clients.
achievement_1: Technical leader and developer for custom software development with Ruby on Rails stack. Usually with one or two tech collaborators.
achievement_2: Technical responsible for most of the projects for different industries like communication, education, professional services, retail, and government.
achievement_3:
achievement_4:
achievement_5:
---
## **<%= resource.data.position_title %>**
<%= resource.data.start_date %> – <%= resource.data.end_date %>. <%= resource.data.company_name %>. (<%= resource.data.office_type %>). <%= resource.data.place %>.

I collaborated to create some Ruby On Rails apps; for the government sector ([Peteiha Trabajo](http://peteiha.corrientes.gov.ar/admin)) and for the education sector ([Mi Cuaderno Digital](https://micuadernodigital.mec.gob.ar/) that included an API REST). For the private sector: in the category of funeral services (Aquaronte), for the category of fumigation services (Zapem), for the category of professional studies (App Novedades). \
I also participated in the development of projects and proposals for different clients. It was generally being the technical leader or the technical responsible.

- **Technical leader** and developer for custom software development with Ruby on Rails stack. Usually with one or two tech collaborators.
- **Technical responsible** for most of the projects for different industries like communication, education, professional services, retail, and government.
