---
position_title: "Web developer, Desktop developer, IT Support"
layout: job
date: 2006-01-01 22:00:00 -0300
start_date: 'Jan 2006'
end_date: 'Jan 2009'
place: Corrientes city, ARG
draft: false
view_more: false
category: jobs
company_name: Freelance developer
office_type: Work from home
company_website:
job_description: During that time, I developed severals website and desktop app. And a lot of IT support.
achievement_1: I was the webmaster of severals websites
achievement_2: I develop some desktop app with MS Visual Basic.
achievement_3: I helped to grow some Cybercafe business
achievement_4:
achievement_5:
---

<%= resource.data.start_date %> – <%= resource.data.end_date %>. <%= resource.data.company_name %>. (<%= resource.data.office_type %>).
<%= resource.data.place %>.

<%= resource.data.job_description %>

<%= "- #{resource.data.achievement_1}" if resource.data.achievement_1 %>
<%= "- #{resource.data.achievement_2}" if resource.data.achievement_2 %>
<%= "- #{resource.data.achievement_3}" if resource.data.achievement_3 %>
<%= "- #{resource.data.achievement_4}" if resource.data.achievement_4 %>
<%= "- #{resource.data.achievement_5}" if resource.data.achievement_5 %>
<%= "- #{resource.data.achievement_6}" if resource.data.achievement_6 %>