---
position_title: "Webmaster"
layout: job
date: 2009-01-01 22:00:00 -0300
start_date: 'Jan 2013'
end_date: 'Jan 2014'
place: Corrientes city, ARG
draft: false
view_more: false
category: jobs
company_name: TEX Consultores Asociados
office_type: Remote
company_website: https://www.facebook.com/texsoluciones
job_description: I collaborated to improve the main web page product of the company. And I helped to grow the social media presence of the products.
achievement_1:
achievement_2:
achievement_3:
achievement_4:
achievement_5:

---

<%= resource.data.start_date %> – <%= resource.data.end_date %>. <%= resource.data.company_name %>. (<%= resource.data.office_type %>).
<%= resource.data.place %>.

<%= resource.data.job_description %>

<%= "- #{resource.data.achievement_1}" if resource.data.achievement_1 %>
<%= "- #{resource.data.achievement_2}" if resource.data.achievement_2 %>
<%= "- #{resource.data.achievement_3}" if resource.data.achievement_3 %>
<%= "- #{resource.data.achievement_4}" if resource.data.achievement_4 %>
<%= "- #{resource.data.achievement_5}" if resource.data.achievement_5 %>
<%= "- #{resource.data.achievement_6}" if resource.data.achievement_6 %>