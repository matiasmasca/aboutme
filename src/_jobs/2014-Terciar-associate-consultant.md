---
position_title: "Web Developer & Associate Consultant"
layout: job
date: 2014-01-01 22:00:00 -0300
start_date: 'Jan 2014'
end_date: 'Jan 2016'
place: Corrientes city, ARG
draft: false
view_more: false
category: jobs
company_name: Terciar Consultores
office_type: Remote
company_website: https://www.facebook.com/texsoluciones
job_description: As a technology consultant, my role was to provide solutions to the needs of our clients. My tasks range from configuring servers to adding value to WordPress or developing web applications with Ruby. Sometimes, I have also to define the requirements, make demos, and training sessions, and even manage customer payments. It depends on the customer’s location and the number of consultants involved in the projects. I developed some Ruby apps for the business and service sector.
achievement_1:
achievement_2:
achievement_3:
achievement_4:
achievement_5:

---

<%= resource.data.start_date %> – <%= resource.data.end_date %>. <%= resource.data.company_name %>. (<%= resource.data.office_type %>).
<%= resource.data.place %>.

<%= resource.data.job_description %>

<%= "- #{resource.data.achievement_1}" if resource.data.achievement_1 %>
<%= "- #{resource.data.achievement_2}" if resource.data.achievement_2 %>
<%= "- #{resource.data.achievement_3}" if resource.data.achievement_3 %>
<%= "- #{resource.data.achievement_4}" if resource.data.achievement_4 %>
<%= "- #{resource.data.achievement_5}" if resource.data.achievement_5 %>
<%= "- #{resource.data.achievement_6}" if resource.data.achievement_6 %>