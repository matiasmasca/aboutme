---
position_title: "Visual Basic Desktop Developer (VB 6) & Product Management"
layout: job
date: 2009-01-01 22:00:00 -0300
start_date: 'Jan 2009'
end_date: 'Jan 2013'
place: Corrientes city, ARG
draft: false
view_more: false
category: jobs
company_name: Optimal Argentina
office_type: On site
company_website:
job_description: I develop a desktop Visual Basic application connected to a MySQL database for the fast food sector. I also collaborated on different tasks in different projects, making up for the lack of specialists.
achievement_1: I was the webmaster of the company websites
achievement_2: I develop some desktop app with MS Visual Basic
achievement_3:
achievement_4:
achievement_5:
---

<%= resource.data.start_date %> – <%= resource.data.end_date %>. <%= resource.data.company_name %>. (<%= resource.data.office_type %>).
<%= resource.data.place %>.

<%= resource.data.job_description %>

<%= "- #{resource.data.achievement_1}" if resource.data.achievement_1 %>
<%= "- #{resource.data.achievement_2}" if resource.data.achievement_2 %>
<%= "- #{resource.data.achievement_3}" if resource.data.achievement_3 %>
<%= "- #{resource.data.achievement_4}" if resource.data.achievement_4 %>
<%= "- #{resource.data.achievement_5}" if resource.data.achievement_5 %>
<%= "- #{resource.data.achievement_6}" if resource.data.achievement_6 %>