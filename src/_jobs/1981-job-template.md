---
position_title: "Full Stack Web Developer"
layout: jobs
start_date: 'Feb 2017'
end_date: 'Ago 2020'
place: Corrientes city, ARG
draft: true
view_more: true
category: jobs
company_name: Terciar Consultores
office_type: Remote
company_website: https://www.duckduckgo.com/
job_description: Here a description about the talk
achievement_1: achievement 1
achievement_2: achievement 2
achievement_3: achievement 3
achievement_4: achievement 4
achievement_5: achievement 5
---
## **<%= resource.data.position_title %>**
<%= resource.data.start_date %> – <%= resource.data.end_date %>. <%= resource.data.company_name %>. (<%= resource.data.office_type %>).
<%= resource.data.place %>.

<%= resource.data.job_description %>

<%= "- #{resource.data.achievement_1}" if resource.data.achievement_1 %>
<%= "- #{resource.data.achievement_2}" if resource.data.achievement_2 %>
<%= "- #{resource.data.achievement_3}" if resource.data.achievement_3 %>
<%= "- #{resource.data.achievement_4}" if resource.data.achievement_4 %>
<%= "- #{resource.data.achievement_5}" if resource.data.achievement_5 %>
<%= "- #{resource.data.achievement_6}" if resource.data.achievement_6 %>