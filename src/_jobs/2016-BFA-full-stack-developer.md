---
position_title: "Web Developer & Associate Consultant"
layout: job
date: 2016-01-01 22:00:00 -0300
start_date: 'Jan 2016'
end_date: 'March 2017'
place: Corrientes city, ARG
draft: false
view_more: false
category: jobs
company_name: BFA.com
office_type: Remote
company_website: https://www.bfa.com
job_description: I did tasks of maintenance, deploying, and developing new features to a Ruby on Rails app, fixing some important issues. Also, I worked as a front-end developer (web layout, html + css) for an internal spin-off. Furthermore I develop some features with Clojure and ClojureScript and I integrated with Third-party APIs (Zendesk, Facebook). As a contractor.
achievement_1:
achievement_2:
achievement_3:
achievement_4:
achievement_5:
---

<%= resource.data.start_date %> – <%= resource.data.end_date %>. <%= resource.data.company_name %>. (<%= resource.data.office_type %>).
<%= resource.data.place %>.

<%= resource.data.job_description %>

<%= "- #{resource.data.achievement_1}" if resource.data.achievement_1 %>
<%= "- #{resource.data.achievement_2}" if resource.data.achievement_2 %>
<%= "- #{resource.data.achievement_3}" if resource.data.achievement_3 %>
<%= "- #{resource.data.achievement_4}" if resource.data.achievement_4 %>
<%= "- #{resource.data.achievement_5}" if resource.data.achievement_5 %>
<%= "- #{resource.data.achievement_6}" if resource.data.achievement_6 %>