---
layout: projects
title: "Mi Cuaderno Digital - spin-off"
date: 2017-02-01 00:00:00 -0300
category: project
locale: multi
tag:
- Ruby on Rails
- Material Design
- Technical Lead
- Database Design
- startup
- spin-off
description: It is a web app, with this api-rest, for communication between teachers
  and guardians. Actually it's used in some public elementary schools of Corrientes
  province, Argentina.
---

**codename**: *Cuaderno Gordo* - the chunky notebook \
(May 2017 → Jun 2020)

<% if site.locale == :en %>
It was a mobile and web app for communication between school teachers and guardians. It was used in some public elementary schools in Corrientes province, Argentina.

I participated in the whole process from the *Agile Inception desk* to the deployment of it on on-premises servers; I collaborated with the product design process. Starting with some low-fi wireframes.

I developed the first version of the MVP app. After some validation, we redesigned and reimplemented some parts of the app but in this phase, I received most of the screens in html + css format to incorporate into the app.

Finally, I designed an API Restful to be consumed for an Android native App.
<% elsif site.locale == :pt %>
Era um aplicativo móvel e web para comunicação entre professores e responsáveis. Foi utilizado em algumas escolas primárias públicas da província de Corrientes, Argentina.

Participei de todo o processo desde o *Agile Inception desk* até a implantação do mesmo em servidores locais; Colaborei com o processo de design do produto. Começando com alguns wireframes de baixa fidelidade.

Desenvolvi a primeira versão do aplicativo MVP. Depois de algumas validações, redesenhamos e reimplementamos algumas partes do aplicativo, mas nesta fase, recebi a maioria das telas no formato html + css para incorporar ao aplicativo.

Por fim, desenvolvi uma API Restful para ser consumida por um aplicativo nativo Android.

<% elsif site.locale == :es %>

Desarrollamos íntegramente una plataforma de comunicación para el sector Educativo, desde el concepto, su diseño y desarrollo, hasta el despliegue; colaborando con la empresa Horeb Capacitaciones quién aportó su conocimiento en el campo, las validaciones tempranas con los usuarios y realizó las capacitaciones a los usuarios finales.

> Mi Cuaderno Digital establece el paradigma donde la escuela va al Alumno. Pone a disposición de la comunidad educativa Herramientas Tecnológicas específicas para maximizar las oportunidades de mejora de los resultados educativos en todos sus niveles.

Diseñamos y construimos, en varios ciclos iterativos, una aplicación que pueda ser utilizada desde cualquier dispositivo: computadoras de escritorio, portátiles, netbooks, tablets y principalmente celulares de diferentes gamas; por ello desarrollar una solución accesible desde un navegador web (o web app) y una aplicación nativa para dispositivos móviles, con el sistema operativo Android.

Además de la aplicación web, diseñamos y desarrollamos una API RESTfull compatible con el estandar JSON:API lo que permite tener acceso a los datos de la plataforma desde diferentes fuentes y brinda la posibilidad de interactuar con sistemas pre-existentes.

Más información en [https://micuadernodigital.com](https://micuadernodigital.com)
<% end %>


