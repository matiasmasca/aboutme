---
title: "Peteiha Trabajo"
layout: projects
date: 2015-06-01 16:18:00 -0300
category: project
locale: multi
description: a web app for the administration of applications and raffle of positions for a government first job program.
externalLink: https://github.com/matiasmasca/
---

**codename**: *Peteiha Trabajo*

Peteiha Work: a web app for the administration of applications and raffle of positions for a government first job program.
