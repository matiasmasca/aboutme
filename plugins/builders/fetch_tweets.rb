class Builders::FetchTweets < SiteBuilder
  def build
    add_resource :posts, "2020-05-17-way-to-go-bridgetown.md" do
        layout :post
        title "Way to Go, Bridgetown!"
        author "rlstevenson"
        content "It's pretty _nifty_ that you can add **new blog posts** this way."
    end

    # tweets = get_tweets("matiasmasca")

    # create a post in a collection per tweet.
  end

  def get_tweets(user_handler)
    # The code below sets the bearer token from your environment variables
    # To set environment variables on Mac OS X, run the export command below from the terminal:
    # export TW_BEARER_TOKEN='YOUR-TOKEN'
    bearer_token = ENV["TW_BEARER_TOKEN"]
    
    # Specify the User screen name that you want to lookup
    user_name = "matiasmasca"

    user_lookup_url = "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=#{user_name}"

    response = connection.get("#{user_lookup_url}", nil,{"Authorization" => "Bearer #{bearer_token}"}) 
    parse_tweets(response.body)
  end

  def parse_tweets(response_body)
    tweets = []
    
    response_body.each do |data|
      hashtags = data.entities["hashtags"][0].dig("text") unless  data.entities["hashtags"].blank?
      tweet = {
        id: data.id,
        created_at: data.created_at,
        text: data.text,
        is_retweet: !data.retweeted_status.blank?,
        lang: data.lang,
        hashtags: hashtags,
        retweet_count: data.retweet_count,
        favorite_count: data.favorite_count,
        public_url: "https://twitter.com/#{data.user["screen_name"]}/status/#{data.id}",
      }

      tweets << tweet 
    end

    tweets
  end
end
