class Builders::Helpers < SiteBuilder
  def build
    helper :locale_to_flag do |locale_param|
      case locale_param
      when  /en/
        "🇺🇲"
      when /es/
        "🇪🇸"
      when /pt/
        "🇧🇷"
      end
    end
  end
end
